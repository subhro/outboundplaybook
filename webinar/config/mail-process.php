<?php 
function Send_Mail($subject, $body, $recipient_name, $recipient_mail, $cc = FALSE, $bcc = FALSE, $attachment = FALSE, $debug = FALSE) {
	include_once( dirname(__FILE__) . "/PHPMailer/class.phpmailer.php" );
	include_once( dirname(__FILE__) . "/PHPMailer/class.smtp.php" );
	$mail = new PHPMailer;
	
	$mail->isSMTP();                                      						// Set mailer to use SMTP
	$mail->Host 		= 'a2plcpnl0905.prod.iad2.secureserver.net';			// Specify main and backup server
	$mail->Port 		= 465;
	$mail->SMTPAuth 	= TRUE;                               					// Enable SMTP authentication
	$mail->Username 	= 'support@outboundplaybook.com';                  		// SMTP username
	$mail->Password 	= 'Wy01sudip';			                           		// SMTP password
	$mail->SMTPSecure 	= 'ssl';				                           		// Enable encryption, 'ssl' also accepted
	
	$mail->From 		= 'support@outboundplaybook.com';
	$mail->FromName 	= 'OutBoundPlayBook';
	
	if ( is_array($recipient_name) && is_array($recipient_mail) ) {
		for ( $i = 0; $i < count($recipient_mail); $i++ ) {
			$mail->addAddress($recipient_mail[$i], $recipient_name[$i]);  		// Add multiple recipient
		}
	} else {
		$mail->addAddress($recipient_mail, $recipient_name);  					// Add a recipient
	}
	
	$mail->addReplyTo('support@outboundplaybook.com', 'OutBoundPlayBook');						// Reply to email and name
	
	if ( $cc ) {																// Add CC recipient
		if ( is_array($cc) ) {
			for ( $i = 0; $i < count($cc); $i++ ) {
				$mail->addCC($cc[$i]);											// Add multiple CC recipient
			}
		} else {
			$mail->addCC($cc);													// Add single CC recipient
		}
	}
	
	if ( $bcc ) {																// Add BCC recipient
		if ( is_array($bcc) ) {
			for ( $i = 0; $i < count($bcc); $i++ ) {
				$mail->addBCC($bcc[$i]);										// Add multiple BCC recipient
			}
		} else {
			$mail->addBCC($bcc);												// Add single BCC recipient
		}
	}
	
	$mail->WordWrap 	= 50;				                                 	// Set word wrap to 50 characters
	
	if ( $attachment ) {
		for ( $i = 0; $i < count($attachment); $i++ ) {
			$mail->addAttachment($attachment[$i]['src'], $attachment[$i]['name']);
		}
	}
	
	$mail->isHTML(TRUE);                                  						// Set email format to HTML
	
	if ( $debug ) {
		$mail->SMTPDebug 	= 2;												// Enable debugging mode
	}
	
	$mail->Subject 		= $subject;
	$mail->Body    		= $body;
	
	if ( !$mail->send() ) {
		return 'Message could not be sent.';
		return 'Mailer Error: ' . $mail->ErrorInfo;
		exit;
	}
	return 'Message has been sent';
}