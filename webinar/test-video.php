<?php
include_once( dirname( __FILE__ ) . "/config/mail-process.php" );
require( dirname( dirname( __FILE__ ) ) . '/wp-load.php' );
global $wpdb;
if (!session_id()) {
    session_start();
}

if( empty( $_GET['_web_token'] ) ) {	
	header("location:" . site_url());
} else {
	$CheckUser = $wpdb->get_row( "SELECT * FROM " . $wpdb->prefix . "webinar WHERE Cust_Unique_ID = '".$_GET['_web_token']."'" );
	if( count($CheckUser) == 0 ) {
		header("location:".site_url());
	} else {
		$msg = '';
		$UniqueID	= $_GET['_web_token'];
		$User 		= $wpdb->get_row( "SELECT * FROM " . $wpdb->prefix . "webinar WHERE Cust_Unique_ID = '".$UniqueID."'" );

		if( isset( $_POST['update_webinar'] ) ) {
			$Website 	 = addslashes($_POST['website']);
			$Company 	 = addslashes($_POST['company']);
			$Designation = addslashes($_POST['designation']);
			$Phone 		 = addslashes($_POST['phone']);

			if( !empty($Website) && !empty($Company) && !empty($Designation) && !empty($Phone) ) {
				$UpdateWebinar = $wpdb->update( 
					$wpdb->prefix . 'webinar',
					array(
						'Cust_Website' 		=> $Website,
						'Cust_Company' 		=> $Company,
						'Cust_Phone'		=> $Phone,
						'Cust_Designation' 	=> $Designation,												
					),
					array(
						'Cust_Unique_ID' => $UniqueID
					)
				);
				header("location:".site_url('/webinar/test-video/?_web_token=' . $UniqueID));
				$msg = '<p class="text-success text-center">Your details updated successfully.</p>';
			} else {
				$msg = '<p class="text-danger text-center">Opps! something went wrong.</p>';
			}
		}
	} ?>

	<!DOCTYPE html>
	<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <title>Webinar Video</title>

	    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
		<link href="<?php echo site_url('/webinar/styles/nanoscroller.css'); ?>" rel="stylesheet">
		<link href="<?php echo site_url('/webinar/styles/style.css'); ?>" rel="stylesheet">

	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	</head>
	<body>
		<div class="container">
			<?php if ( $User->Cust_WebinarDate > date( "Y-m-d H:i:s" ) ) { ?>
				<?php if( empty($User->Cust_Website) ) { ?>
					<video width="100%" height="100%" id="intro_webinar_player">
				<?php } else { ?>
					<video width="100%" height="100%" loop autoplay playsinline id="intro_webinar_player">
				<?php } ?>
					<source src="<?php echo site_url('/webinar/videos/VfE_html5.mp4'); ?>" type="video/mp4">
					<source src="<?php echo site_url('/webinar/videos/VfE_html5.ogg'); ?>" type="video/ogg">
					<source src="<?php echo site_url('/webinar/videos/VfE_html5.webm'); ?>" type="video/webm">
					Your browser does not support HTML5 video.
				</video>
			<?php } else { ?>
				<div class="video-wrap" id="webinar_container">				
					<video width="100%" height="100%" controls="true" autoplay playsinline id="webinar_player" data-uniqueid="<?php echo $UniqueID; ?>">
						<source src="<?php echo site_url('/webinar/videos/VfE_html5.mp4'); ?>" type="video/mp4">
						<source src="<?php echo site_url('/webinar/videos/VfE_html5.ogg'); ?>" type="video/ogg">
						<source src="<?php echo site_url('/webinar/videos/VfE_html5.webm'); ?>" type="video/webm">
						Your browser does not support HTML5 video.
					</video>

					<div class="messages"></div>
					<div class="live-icon">
						<span class="blink"></span> LIVE
					</div>
					<div class="comment-count">
						<i class="fa fa-user" aria-hidden="true"></i> <span id="users">34</span>
					</div>
					<div class="fullscreen">
						<i id="enter_fullscreen_btn" class="fa fa-expand" aria-hidden="true"></i>
						<i id="exit_fullscreen_btn" class="fa fa-compress hide" aria-hidden="true"></i>
					</div>

					<div class="charts">
						<div id="pie3d"></div>
						<div id="question"></div>
					</div>
					<div class="buy-button hide">
						<button type="button" class="btn btn-lg btn-success" data-toggle="modal" data-target="#buyModal">BUY NOW!</button>
					</div>
					<div class="modal fade" id="buyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="buyModalLabel">Payment Information</h4>
								</div>
								<div class="modal-body">
									<form action="<?php echo site_url('/webinar/charge/'); ?>" method="post" id="buy-form">
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
													<label for="card_no">Credit Card Number</label>
													<div class="row">
														<div class="col-sm-6">
															<input type="text" class="card-number form-control" id="card_no" required data-msg-required="cannot be blank">
														</div>
														<div class="col-sm-6">                      	
															<span class="cb-cards hidden-xs">                                        
																<span class="visa">  </span>                                        
																<span class="mastercard">  </span>                                        
																<span class="american_express">  </span>
																<span class="discover">  </span>
															</span> 
														</div>
													</div>
													<small for="card_no" class="text-danger"></small>
												</div>
											</div>                                                             
										</div>
										<div class="row">                
											<div class="col-sm-6">                                	
												<div class="form-group">
													<label for="expiry_month">Card Expiry</label>
													<div class="row">
														<div class="col-xs-6">
															<select class="card-expiry-month form-control" id="expiry_month" required data-msg-required="empty">
																<option selected>01</option>
																<option>02</option>
																<option>03</option>
																<option>04</option>
																<option>05</option>
																<option>06</option>
																<option>07</option>
																<option>08</option>
																<option>09</option>
																<option>10</option>
																<option>11</option>
																<option>12</option>
															</select>
														</div>
														<div class="col-xs-6">
															<select class="card-expiry-year form-control" id="expiry_year" required data-msg-required="empty">
																<option>2013</option>
																<option>2014</option>
																<option>2015</option>
																<option>2016</option>
																<option>2017</option>
																<option>2018</option>
																<option>2019</option>
																<option selected="">2020</option>
																<option>2021</option>
																<option>2022</option>
																<option>2023</option>
															</select>
														</div>
													</div> 
													<small for="expiry_month" class="text-danger"></small>
													<small for="expiry_year" class="text-danger"></small>
												</div>                                       
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label for="ccv">CCV</label>
													<div class="row">                                    	
														<div class="col-xs-6">
															<input type="text" class="card-cvc form-control" id="ccv" placeholder="CCV" required data-msg-required="empty">
														</div>
														<div class="col-xs-6">                                            	
															<h6 class="cb-cvv"><small>(Last 3-4 digits)</small></h6>
														</div>
													</div>
													<small for="ccv" class="text-danger"></small>
												</div>
											</div>                                      
										</div>                      
										<div class="row">
											<div class="col-sm-12">
												<hr><p>By clicking Subscribe, you agree to our privacy policy and terms of service.</p>
												<p><small class="text-danger" style="display:none;">There were errors while submitting</small></p>
												<button type="submit" class="btn btn-primary btn-lg">Subscribe</button>&nbsp;&nbsp;&nbsp;&nbsp;
												<span class="subscribe_process process" style="display:none;">Processing&hellip;</span>
											</div>
										</div>     
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="comments">
						<div class="comment-inner">
							<div class="nano">
								<ul class="list-unstyled nano-content" id="comment-list"></ul>
							</div>
						</div>
						<div class="comment-input">
							<input type="text" class="form-control" id="comment" placeholder="Write Comments">
						</div>
					</div>
				</div>
			<?php } ?>

			<?php if( empty($User->Cust_Website) ) { ?>
				<div id="webinarModal" class="modal fade" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
					    	<div class="modal-header">
								<h4 class="modal-title" id="buyModalLabel">Update Following Details</h4>
					    	</div>
					    	<div class="modal-body">
					    		<form method="post" action="" id="WebinarUpdateForm">
									<?php echo $msg; ?>
									<div class="form-group">
										<label for="website">Website:</label>
										<input class="form-control" type="text" name="website" id="website" placeholder="Enter Website" />
									</div>
									<div class="form-group">
										<label for="company">Company:</label>
										<input class="form-control" type="text" name="company" id="company" placeholder="Enter Company Name" />
									</div>									
									<div class="form-group">
										<label for="phone">Phone:</label>
										<input class="form-control" type="text" name="phone" id="phone" placeholder="Enter Phone" />
									</div>
									<div class="form-group">
										<label for="designation">Designation:</label>
										<input class="form-control" type="text" name="designation" id="designation" placeholder="Enter Designation" />
									</div>
									<div class="form-group">
										<button type="submit" name="update_webinar" class="btn btn-primary">Update</button>
									</div>
								</form>
					    	</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.min.js"></script>
		<script src="<?php echo site_url('/webinar/scripts/bootstrap-notify.min.js'); ?>"></script>
		<script src="<?php echo site_url('/webinar/scripts/jquery.nanoscroller.min.js'); ?>"></script>
		<script src="<?php echo site_url('/webinar/scripts/highcharts.js'); ?>"></script>
		<script src="<?php echo site_url('/webinar/scripts/highcharts-3d.js'); ?>"></script>
		<script type="text/javascript">var siteurl = '<?php echo site_url('/webinar'); ?>';</script>
		<script src="<?php echo site_url('/webinar/scripts/function.js'); ?>"></script>
		<script src="<?php echo site_url('/webinar/scripts/test-dummy.js'); ?>"></script>		
		<script src="<?php echo site_url('/webinar/scripts/test-app.js'); ?>"></script>
		<script type="text/javascript">
		$(document).ready(function() {
			var WebinarVideoPlayer = document.getElementById("webinar_player");
			WebinarVideoPlayer.currentTime = <?php echo $User->Cust_Webinar_TimeSpan; ?>;

			<?php if( empty($User->Cust_Website) ) { ?>
				setTimeout(function() {
					$('#webinarModal').modal('show');
				}, 5000);
			<?php } ?>
		});		
		</script>
	</body> 
	</html>
<?php 
}
?>