<?php 
include_once(dirname(__FILE__) . "/config/mail-process.php");
require(dirname(dirname(__FILE__)) . '/wp-load.php');

global $wpdb;
if ( !session_id() ) session_start();

if ( empty($_SESSION) ) { 
    header("location:" . site_url());
    exit();
} else {
    $EmailID     = $_SESSION['_webinar_data']['Webinar_EmailID'];
    $CallBackURL = $_SESSION['_webinar_data']['Webinar_Callback_URL'];
    $UserData = $wpdb->get_row("SELECT * FROM `" . $wpdb->prefix . "webinar` WHERE `Cust_Email` = '" . $EmailID . "'");
    if ( !empty($UserData->Cust_Phone) ) {
        $UserPhone = $UserData->Cust_Phone;
    } else {
        $UserPhone = '';
    }
} ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Thank You - OutboundPlaybook</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo site_url('/webinar/styles/font-awesome.min.css'); ?>">

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center"> 
                    <h2 class="lead text-success">You've successfully scheduled for Webinar!</h2>
                    <p class="lead">Your Webinar information has been sent to your email address.</p> 
                </div>
                <img src="<?php echo site_url('/webinar/images/success.png');?>" class="img-responsive center-block"><br> 
                <div class="text-center">            
                    <p class="lead">Please check your email to get Webinar Schedule Date & Time.</p>
                    <div class="col-md-8 col-md-offset-2">
                        <div class="form-group">
                            <video width="100%" height="100%" autoplay playsinline controls id="intro_webinar_player">
                                <source src="<?php echo site_url('/webinar/videos/MobileApp-CTA.mp4'); ?>" type="video/mp4">
                                <source src="<?php echo site_url('/webinar/videos/MobileApp-CTA.ogg'); ?>" type="video/ogg">
                                <source src="<?php echo site_url('/webinar/videos/MobileApp-CTA.webm'); ?>" type="video/webm">
                                Your browser does not support HTML5 video.
                            </video>
                        </div>
                    </div>
                    <div class="col-md-4 col-md-offset-4">
                        <div class="form-group">
                            <label>Enter your Mobile No to get free app</label>
                            <div class="input-group">
                                <input type="hidden" id="webinar_email" name="webinar_email" value="<?php echo $EmailID;?>" />
                                <input type="text" placeholder="Enter Mobile No with country code" class="form-control" id="webinar_phone" name="webinar_phone" value="<?php echo $UserPhone; ?>" />
                                <span class="input-group-btn">
                                    <?php if( $CallBackURL != '' ) { ?>
                                        <a href="<?php echo $CallBackURL; ?>" class="btn btn-default">Back</a>
                                    <?php } ?>
                                    <button type="button" id="add_webinar_phone" class="btn btn-primary">Submit</button>
                                </span>
                            </div>
                            <p class="addPhoneMsg"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>var siteurl = '<?php echo site_url('/webinar'); ?>';</script>
    <script src="<?php echo site_url('/webinar/scripts/function.js'); ?>"></script>
</body>
</html>