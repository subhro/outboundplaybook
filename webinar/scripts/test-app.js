var video_con = document.getElementById("webinar_container");
$(document).on('click', '#enter_fullscreen_btn', function(e) {
    e.preventDefault();

    if (video_con.requestFullscreen) {
        video_con.requestFullscreen();
    } else if (video_con.mozRequestFullScreen) {
        video_con.mozRequestFullScreen();
    } else if (video_con.webkitRequestFullscreen) {
        video_con.webkitRequestFullscreen();
    }
    $('#exit_fullscreen_btn').removeClass('hide');
    $(this).addClass('hide');
});

$(document).on('click', '#exit_fullscreen_btn', function(e) {
    e.preventDefault();

    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
    }
    $('#enter_fullscreen_btn').removeClass('hide');
    $(this).addClass('hide');
});

$(document).on('click', '.option_1', function() {
    var p = $(this).data('index');
    $('.flying-icons').append('<span class="fade-right" style="color:' + Dummy.Polls[p].color1 + ';"><i class="fa ' + Dummy.Polls[p].icon1 + '"></i> ' + Dummy.Polls[p].option1 + '</span>');
    $(".fade-right").last().animate({left:Math.floor(Math.random()*(100-80+1)+80) + '%', bottom:Math.floor(Math.random()*(100-50+1)+50) + '%', opacity:"show"}, 5000).fadeOut();
});
$(document).on('click', '.option_2', function() {
    var p = $(this).data('index');
    $('.flying-icons').append('<span class="fade-right" style="color:' + Dummy.Polls[p].color2 + ';"><i class="fa ' + Dummy.Polls[p].icon2 + '"></i> ' + Dummy.Polls[p].option2 + '</span>');
    $(".fade-right").last().animate({left:Math.floor(Math.random()*(100-80+1)+80) + '%', bottom:Math.floor(Math.random()*(100-50+1)+50) + '%', opacity:"show"}, 5000).fadeOut();
});
$(document).on('click', '.option_3', function() {
    var p = $(this).data('index');
    $('.flying-icons').append('<span class="fade-right" style="color:' + Dummy.Polls[p].color3 + ';"><i class="fa ' + Dummy.Polls[p].icon3 + '"></i> ' + Dummy.Polls[p].option3 + '</span>');
    $(".fade-right").last().animate({left:Math.floor(Math.random()*(100-80+1)+80) + '%', bottom:Math.floor(Math.random()*(100-50+1)+50) + '%', opacity:"show"}, 5000).fadeOut();
});
$(document).on('click', '.option_4', function() {
    var p = $(this).data('index');
    $('.flying-icons').append('<span class="fade-right" style="color:' + Dummy.Polls[p].color4 + ';"><i class="fa ' + Dummy.Polls[p].icon4 + '"></i> ' + Dummy.Polls[p].option4 + '</span>');
    $(".fade-right").last().animate({left:Math.floor(Math.random()*(100-80+1)+80) + '%', bottom:Math.floor(Math.random()*(100-50+1)+50) + '%', opacity:"show"}, 5000).fadeOut();
});

$(document).ready(function() {
    "use strict";

    $(".nano").nanoScroller();
});

var users, comments;
var notiFlag = false, qstnFlag = false, pollFlag = false, pieFlag = false;

function GeneratePie3DChart(data) {
    if ( pieFlag == false ) {
        pieFlag = true;
        $('#pie3d').show();
        $('#question').html('');
        Highcharts.chart('pie3d', {
            colors: ['#3399FF', '#FF0000', '#F9C840', '#19EA3C'],
            center: ["50%", "50%"],
            credits: {
                enabled: false
            },
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 70,
                    beta: 0
                },
                backgroundColor:'rgba(0, 0, 0, 0.0)'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '<b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name} ({point.percentage:.1f}%)'
                    }
                }
            },
            series: [{
                type: 'pie',
                data: data
            }]
        });
    }
}

function GeneratePeople(data) {
    users = setInterval(function() {
        $('#users').text(Math.floor(Math.random()*(data.max-data.min+1)+data.min));
    }, 5000);
}

function GenerateNotify(data) {
    if ( notiFlag == false ) {
        notiFlag = true;
        new Audio(data.audio).play();
        $.notify({
            icon: 'glyphicon glyphicon-bookmark',
            message: data.message,
        },{
            type: data.type,
            placement: {
                from: "top",
                align: "left"
            },
            offset: 0,
            newest_on_top: true,
            element: $('#webinar_container .messages'),
            template: '<div class="alert alert-{0}">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                '<span data-notify="icon"></span> ' +
                '<span data-notify="message">{2}</span>' +
            '</div>' 
        });
    }
}

function GeneratePolls(data, index) {
    if ( qstnFlag == false ) {
        qstnFlag = true;
        $('#pie3d').hide();
        var html = '<div class="btn-group">';
        if ( data.option1 ) {
            html += '<button type="button" data-index="' + index + '" style="background:' + data.color1 + ';border-color:' + data.color1 + '" class="btn btn-lg btn-primary option_1"><i class="fa ' + data.icon1 + '"></i> ' + data.option1 + '</button>';
        }
        if ( data.option2 ) {
            html += '<button type="button" data-index="' + index + '" style="background:' + data.color2 + ';border-color:' + data.color2 + '" class="btn btn-lg btn-primary option_2"><i class="fa ' + data.icon2 + '"></i> ' + data.option2 + '</button>';
        }
        if ( data.option3 ) {
            html += '<button type="button" data-index="' + index + '" style="background:' + data.color3 + ';border-color:' + data.color3 + '" class="btn btn-lg btn-primary option_3"><i class="fa ' + data.icon3 + '"></i> ' + data.option3 + '</button>';
        }
        if ( data.option4 ) {
            html += '<button type="button" data-index="' + index + '" style="background:' + data.color4 + ';border-color:' + data.color4 + '" class="btn btn-lg btn-primary option-4"><i class="fa ' + data.icon4 + '"></i> ' + data.option4 + '</button>';
        }
        html += '</div>';
        $('#question').html(html).animate({
            left: 0,
            opacity: 1
        }, 1500);
    }
}

function GenerateComments(data) {
    if ( pollFlag == false ) {
        var i = 0;
        pollFlag = true;
        comments = setInterval(function() {
            if ( i < data.length ) {
                $('#comment-list').append('<li class="notification">\
                    <div class="media">\
                        <div class="media-left">\
                            <div class="media-object">\
                                <img width="50" src="' + data[i].picture + '" class="img-circle" alt="Name">\
                            </div>\
                        </div>\
                        <div class="media-body">\
                            <strong class="notification-title">' + data[i].name + '</strong>\
                            <p class="notification-desc">' + data[i].comment + '</p>\
                        </div>\
                    </div>\
                </li>').slideDown();
                $(".nano").nanoScroller();
                $(".nano").nanoScroller({ scroll: 'bottom' });
                i++;
            }
        }, 2000);
    }
}

function ClearAllArea() {

}
var webinarVideo = document.getElementById("webinar_player");
webinarVideo.addEventListener('timeupdate', function() {
    var curTime = parseInt(webinarVideo.currentTime);
    if ( curTime == 42 ) {
        GeneratePeople(Dummy.People[0]);
    }

    if ( curTime == 58 ) {
        GenerateNotify(Dummy.Notify[0]);
    }
    if ( curTime == 59 ) {
        notiFlag = false;
    }
    
    if ( curTime == 62 ) {
        GeneratePolls(Dummy.Polls[0], 0);
    }
    if ( curTime == 63 ) {
        qstnFlag = false;
    }
    
    if ( curTime == 75 ) {
        GenerateComments(Dummy.Comments);
    }
    if ( curTime == 76 ) {
        pollFlag = false;
    }

    if ( curTime == 83 ) {
        GeneratePie3DChart(Dummy.Pie[0].data);
    }
    if ( curTime == 84 ) {
        pieFlag = false;
    }

    if ( curTime == 124 ) {
        GeneratePolls(Dummy.Polls[1], 1);
        GeneratePeople(Dummy.People[1]);
    }
    if ( curTime == 125 ) {
        qstnFlag = false;
    }

    if ( curTime == 138 ) {
        GenerateComments(Dummy.Comments);
    }
    if ( curTime == 139 ) {
        pollFlag = false;
    }

    if ( curTime == 155 ) {
        GeneratePie3DChart(Dummy.Pie[1].data);
    }
    if ( curTime == 156 ) {
        pieFlag = false;
    }

    if ( curTime == 175 ) {
        $('.buy-button').removeClass('hide');
    }
});

webinarVideo.addEventListener('ended', function() {
    $('.live-icon').find('span').removeClass('blink');
    $('#comment').attr('disabled', 'disabled');
    clearTimeout(users);
    clearTimeout(comments);
});

// Comment Post by Viewer
$(document).on('keypress', '#comment', function(e) {
    if ( e.keyCode == 13 ) {
        $('#comment-list').append('<li class="notification">\
            <div class="media">\
                <div class="media-left">\
                    <div class="media-object">\
                        <img width="50" src="' + siteurl + '/webinar/images/avatar5.png" class="img-circle" alt="Name">\
                    </div>\
                </div>\
                <div class="media-body">\
                    <strong class="notification-title">Me</strong>\
                    <p class="notification-desc">' + $(this).val() + '</p>\
                </div>\
            </div>\
        </li>').slideDown();
        $(this).val('');
        $(".nano").nanoScroller();
        $(".nano").nanoScroller({ scroll: 'bottom' });
    }
});