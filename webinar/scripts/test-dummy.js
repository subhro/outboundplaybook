var Dummy = {
    People: [
        {
            max : 40,
            min : 34
        }, {
            max : 55,
            min : 41
        }
    ],
    Comments: [
        {
            picture : siteurl + '/webinar/images/avatar5.png',
            name    : 'Josh Spoon',
            comment : 'Hey Soumitra, how are you doing?'
        }, {
            picture : siteurl + '/webinar/images/avatar.png',
            name    : 'Daven Michaels',
            comment : 'What’s written on the whiteboard?'
        }, {
            picture : siteurl + '/webinar/images/avatar5.png',
            name    : 'Beejel Parmar',
            comment : 'could you speak louder?'
        }, {
            picture : siteurl + '/webinar/images/avatar04.png',
            name    : 'Yogi Parmar',
            comment : 'Cold calling is dead- what do you think?'
        }, {
            picture : siteurl + '/webinar/images/avatar5.png',
            name    : 'Liz Hurley',
            comment : 'We are 100% outbound company?'
        }, {
            picture : siteurl + '/webinar/images/avatar.png',
            name    : 'Jane Austin',
            comment : 'What outbound prospecting tools do you recommend?'
        }, {
            picture : siteurl + '/webinar/images/avatar04.png',
            name    : 'Art Matuschat',
            comment : 'Is inbound cheaper than outbound?'
        }, {
            picture : siteurl + '/webinar/images/avatar.png',
            name    : 'Sudip Sen',
            comment : 'I am from India- we are selling to the USA'
        }, {
            picture : siteurl + '/webinar/images/avatar5.png',
            name    : 'Surajit Pramanik',
            comment : 'We are a web development company. How do I grow my business through outbound sales?'
        }, {
            picture : siteurl + '/webinar/images/avatar2.png',
            name    : 'Sonia',
            comment : 'Day I love outbound sales'
        }, {
            picture : siteurl + '/webinar/images/avatar3.png',
            name    : 'Taniya Sen',
            comment : 'What CRM do you use in your business?'
        }
    ],
    Notify: [
        {
            message : 'Presenter will ask a question',
            type    : 'info',
            audio   : siteurl + '/webinar/audios/sound.mp3'
        }, {
            message: 'ibero hendrerit erat, id',
            type    : 'danger',
            audio   : siteurl + '/webinar/audios/sound.mp3'
        }, {
            message: 'adipiscing elit. Suspendisse et turpis sed',
            type    : 'success',
            audio   : siteurl + '/webinar/audios/sound.mp3'
        }, {
            message: 'hendrerit erat, id blandit est lorem a tellus. Vestibulum tin',
            type    : 'warning',
            audio   : siteurl + '/webinar/audios/sound.mp3'
        }
    ],
    Polls: [
        {
            option1  : 'YES',
            option2  : 'NO',
            option3  : '',
            option4  : '',
            icon1    : 'fa-thumbs-up',
            icon2    : 'fa-thumbs-down',
            icon3    : '',
            icon4    : '',
            color1   : '#3399FF',
            color2   : '#FF0000',
            color3   : '#F9C840',
            color4   : '#19EA3C'
        }, {
            option1  : 'Cold Emailing',
            option2  : 'Cold Calling',
            option3  : 'Social Media',
            option4  : '',
            icon1    : 'fa-envelope',
            icon2    : 'fa-volume-control-phone',
            icon3    : 'fa-comments-o',
            icon4    : '',
            color1   : '#3399FF',
            color2   : '#FF0000',
            color3   : '#F9C840',
            color4   : ''
        }
    ],
    Pie: [
        {
            data : [
                ['Yes', 85.0], ['No',  15.0],
            ]
        }, {
            data : [
                ['Email', 54.0],
                ['Phone', 32.0],
                ['Social', 14.0]
            ]
        }
    ]
};