$(document).ready(function() {
	"use strict";

    // Webinar Signup Validation Script
    if ( $('#WebinarSignupForm').length > 0 ) {
    	$('#WebinarSignupForm').bootstrapValidator({
            live: 'enabled',
            excluded: [':disabled'],
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                first_name: {
                    validators: {
                        notEmpty: {
                            message: 'Please Enter your First Name'
                        },
                        regexp: {
                            regexp: /^[A-Za-z\s]+$/i,
                            message: 'Your fist name doesn\'t contain any special character or numbers'
                        },
    					stringLength: {
                            min: 3,
                            max: 30,
                            message: 'The first name must be more than 3 and less than 30 characters long'
                        },
                    }
                },
                last_name: {
                    validators: {
                        notEmpty: {
                            message: 'Please Enter your Last Name'
                        },
                        regexp: {
                            regexp: /^[A-Za-z\s]+$/i,
                            message: 'Your last name doesn\'t contain any special character or numbers'
                        },
    					stringLength: {
                            min: 3,
                            max: 30,
                            message: 'The last name must be more than 3 and less than 30 characters long'
                        },
                    }
                },
                email_id: {
                    validators: {
                        notEmpty: {
                            message: 'Email Address is required and cannot be empty'
                        },
                        regexp: {
                            regexp: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                            message: 'The input is not a valid email address'
                        },
                        remote: {
                            type: 'POST',
                            url: siteurl + '/ajax/?case=CheckUserDataExist',
                            message: 'The Email is already used'
                        }
                    }
                }
            }   
        }).on('status.field.bv', function(e, data) {
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e, data) {
            e.preventDefault();

            var $form = $(e.target);
            var bv = $form.data('bootstrapValidator');
            $.post($form.attr('action'), $form.serialize(), function(result) {
                if ( result.code == 0 ) {
                    top.window.location = result.result;
                } else {
                    $.notify({
                        icon: 'glyphicon glyphicon-info',
                        message: result.result,
                    },{
                        type: 'danger',
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        z_index: 9999,
                    });
                }
            }, 'json');
        });
    }
    // End of Script

    // Schedule Validation Script
    if ( $('#WebinarScheduleForm').length > 0 ) {
        $('#WebinarScheduleForm').bootstrapValidator({
            live: 'enabled',
            excluded: [':disabled'],
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                schedule: {
                    validators: {
                        notEmpty: {
                            message: 'Select Schedule date from the list'
                        }
                    }
                }
            }   
        }).on('status.field.bv', function(e, data) {
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e, data) {
            e.preventDefault();

            var $form = $(e.target);
            $form[0].submit();
        });
    }
    // End of Script

    // Webinar Update Form Validation Script
    if ( $('#WebinarUpdateForm').length > 0 ) {
        $('#WebinarUpdateForm').bootstrapValidator({
            live: 'enabled',
            excluded: [':disabled'],
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                website: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter Website address'
                        },
                        uri: {
                            message: 'The Website address is not valid'
                        }
                    }
                }
            }   
        }).on('status.field.bv', function(e, data) {
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e, data) {
            e.preventDefault();
            
            var $form = $(e.target);
            var bv = $form.data('bootstrapValidator');
            $.post($form.attr('action'), $form.serialize(), function(result) {
                if ( result.code == 0 ) {
                    $.notify({
                        icon: 'glyphicon glyphicon-info',
                        message: result.result,
                    },{
                        type: 'success',
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        z_index: 9999,
                    });
                    $('#webinarModal').modal('hide');
                } else {
                    $.notify({
                        icon: 'glyphicon glyphicon-info',
                        message: result.result,
                    },{
                        type: 'danger',
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        z_index: 9999,
                    });
                }
            }, 'json');
        });
    }
    // End of Script

    // Webinar Video Statuse Script    
    if( $("#webinar_player").length > 0 ) {
        var WebinarVideo    = document.getElementById("webinar_player");        
        var UserUniqueID    = $('#webinar_player').data('uniqueid');        
        
        setInterval(function() {
            var CurrentTime = parseInt(WebinarVideo.currentTime);
            $.ajax({
                type    : "POST",
                cache   : false,
                url     : siteurl + "/ajax/?case=WebinarVideoStatus",
                data    : "UserUniqueID=" + UserUniqueID + "&CurrentTime=" + CurrentTime,
                success : function(data) {
                    // Do Nothing
                }
            });
        }, 5000);
        
        WebinarVideo.addEventListener('ended', function() {            
            $.ajax({
                type    : "POST",
                cache   : false,
                url     : siteurl + "/ajax/?case=MissedWebinar",
                data    : "UserUniqueID=" + UserUniqueID,
                success : function(data) {
                    // Do Nothing
                }
            });
        });
    }
    // End of Script

    // Add Phone on Thank You Page
    $(document).on('click', '#add_webinar_phone', function(e) {
        e.preventDefault();

        $('.addPhoneMsg').empty();
        var phone   = $('#webinar_phone').val();
        var email   = $('#webinar_email').val();
        var regex   = /^[0-9\s]+$/;
        
        if( (phone == "" || phone == false) ) {
            $('.addPhoneMsg').text('Phone cannot be empty!').addClass('text-danger').removeClass('text-success');
            return false;
        } else {    
            $.ajax({
                type        : 'POST',
                cache       : false,
                url         : siteurl + "/ajax/?case=AddWebinarPhone",
                data        : {
                    'Phone'     : phone,
                    'Email'     : email
                },
                success     : function(data) {
                    if ( data == 0 ) {
                        $('.addPhoneMsg').text('Something went wrong, please try again.').addClass('text-danger').removeClass('text-success');
                    } else {
                        $('.addPhoneMsg').text('Phone number added successfully.').addClass('text-success').removeClass('text-danger');
                        setTimeout(function() {
                            $('.addPhoneMsg').text('');
                        }, 5000);
                    }
                }
            });
        }
    });
    // End of Script

    if ( $('#WebinarTimer').length > 0 ) {
        var x = setInterval(function() {
            $.post(siteurl + "/ajax/?case=GetCurrentPHPDateTime", {}, function(result) {
                var now = new Date(result.datetime).getTime();
                var distance = countDownDate - now;
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                document.getElementById("WebinarTimer").innerHTML = minutes + "M " + seconds + "S";
                if ( distance < 0 ) {
                    clearInterval(x);
                    window.location.href = siteurl + '/video/?_web_token=' + UniqueID;
                }
            }, 'json');
        }, 1000);
    }

    // Add Phone Number
    if ( $('#WebinarAddPhone').length > 0 ) {
        $('#WebinarAddPhone').bootstrapValidator({
            live: 'enabled',
            excluded: [':disabled'],
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                phone: {
                    validators: {
                        notEmpty: {
                            message: 'Please add Phone number for get App link'
                        },
                        integer: {
                            message: 'Phone number should be only in number'
                        },
                        stringLength: {
                            min: 10,
                            max: 10,
                            message: 'The number must 10 characters'
                        }
                    }
                }
            }   
        }).on('status.field.bv', function(e, data) {
            data.bv.disableSubmitButtons(false);
        }).on('success.form.bv', function(e, data) {
            e.preventDefault();

            $('.addPhoneMsg').empty();
            var phone   = $('#phone').val();
            var email   = $('#email').val();            
            
            $.ajax({
                type        : 'POST',
                cache       : false,
                url         : siteurl + "/ajax/?case=AddWebinarPhone",
                data        : {
                    'Phone'     : phone,
                    'Email'     : email
                },
                success     : function(data) {
                    if ( data == 0 ) {
                        $('.addPhoneMsg').text('Something went wrong, please try again.').addClass('text-danger').removeClass('text-success');
                    } else {
                        $('.addPhoneMsg').text('Phone number added successfully.').addClass('text-success').removeClass('text-danger');
                        setTimeout(function() {
                            $('.addPhoneMsg').text('');
                            $('#AddPhoneModal').modal('hide');
                        }, 5000);
                    }
                }
            });
        });
    }
    // End of Script

    // Buy Button Script
    if( $('#buyButton').length > 0 ) {
        $('#buyButton').on('click', function(e) {
            var uniqueid   = $(this).attr('lang');
            
            $.ajax({
                type        : 'POST',
                cache       : false,
                url         : siteurl + "/ajax/?case=BuyNowWebinar",
                data        : {
                    'UniqueID' : uniqueid
                },
                success     : function(data) {
                    if( data == 0 ) {
                        alert('Something went wrong, please try again.');
                    } else {                        
                        setTimeout(function() {
                           window.location.href = data; 
                        }, 1000);
                    }
                }
            });
        });        
    }
    // End of Script
});