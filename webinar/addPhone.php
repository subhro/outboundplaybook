<?php 
include_once(dirname(__FILE__) . "/config/mail-process.php");
require(dirname(dirname(__FILE__)) . '/wp-load.php');

global $wpdb;

if ( empty($_GET['_web_token']) ) {	
	header("location:" . site_url());
	exit();
} else {
	$UniqueID	= addslashes($_GET['_web_token']);
	$User 		= $wpdb->get_row("SELECT * FROM `" . $wpdb->prefix . "webinar` WHERE `Cust_Unique_ID` = '".$UniqueID."'");
	if ( count($User) == 0 ) {
		header("location:" . site_url());
		exit();
	} else {		
		$Email = $User->Cust_Email;
	}
} ?>
	
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Webinar Video</title>

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo site_url('/webinar/styles/nanoscroller.css'); ?>" rel="stylesheet">
	<link href="<?php echo site_url('/webinar/styles/style.css'); ?>" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-8"><br />
				<video width="100%" height="100%" autoplay playsinline controls id="intro_webinar_player">
					<source src="<?php echo site_url('/webinar/videos/MobileApp-CTA.mp4'); ?>" type="video/mp4">
					<source src="<?php echo site_url('/webinar/videos/MobileApp-CTA.ogg'); ?>" type="video/ogg">
					<source src="<?php echo site_url('/webinar/videos/MobileApp-CTA.webm'); ?>" type="video/webm">
					Your browser does not support HTML5 video.
				</video>
			</div>
			<div class="col-md-4">
				<h2>Welcome to Webinar</h2>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
			</div>
			<div class="col-md-12">
				<div class="text-center">
					<h2>Webinar will start within: </h2>
					<h1 id="WebinarTimer"></h1>
				</div>
			</div>
		</div>

		<?php if( empty($User->Cust_Phone) ) { ?>
			<div id="AddPhoneModal" class="modal fade" tabindex="-1" data-keyboard="false" data-backdrop="static" role="dialog" aria-labelledby="AddPhoneModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
				    	<div class="modal-header">
							<h4 class="modal-title" id="AddPhoneModalLabel">Update Phone Number</h4>
				    	</div>
				    	<div class="modal-body">
				    		<form method="post" action="" id="WebinarAddPhone">								
								<div class="form-group">
									<label for="phone">Phone:</label>
									<input class="form-control" type="text" name="phone" id="phone" placeholder="Enter Phone" />
								</div>																
								<div class="form-group">
									<div class="text-left">
										<p class="addPhoneMsg"></p>
									</div>
									<div class="text-right">
										<input type="hidden" name="email" id="email" value="<?php echo $Email; ?>" />
										<button type="submit" name="add_phone" class="btn btn-primary">Save</button>
									</div>
								</div>
							</form>
				    	</div>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.min.js"></script>
	<script src="<?php echo site_url('/webinar/scripts/bootstrap-notify.min.js'); ?>"></script>
	<script type="text/javascript">var siteurl = '<?php echo site_url('/webinar'); ?>';</script>
	<script type="text/javascript">
	var UniqueID = '<?php echo $UniqueID; ?>';
	var countDownDate = new Date("<?php echo $User->Cust_WebinarDate; ?>").getTime();
	</script>
	<script src="<?php echo site_url('/webinar/scripts/function.js'); ?>"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		<?php if( empty($User->Cust_Phone) ) { ?>
			setTimeout(function() {
				$('#AddPhoneModal').modal('show');
			}, 5000);
		<?php } ?>
	});
	</script>

</body> 
</html>