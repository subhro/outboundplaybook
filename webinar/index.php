<?php
require(dirname(dirname(__FILE__)) . '/wp-load.php');

global $wpdb;

if ( empty($_GET['_web_token']) ) {
	header("location:" . site_url());
	exit();
} else {
	$UserUniqueID	= addslashes($_GET['_web_token']);
	$GetUserDetails = $wpdb->get_row("SELECT * FROM `" . $wpdb->prefix . "webinar` WHERE `Cust_Unique_ID` = '" . $UserUniqueID . "'");

	if ( strtotime(date('Y-m-d H:i:s')) > strtotime($GetUserDetails->Cust_WebinarDate) ) {
		header("location:" . site_url('/webinar/video/?_web_token=' . $UserUniqueID));
		exit();
	} else {
		header("location:" . site_url('/webinar/intro/?_web_token=' . $UserUniqueID));
		exit();
	}
} ?>