'use strict';

app.directive('loginForm', function() {
	return{
		restrict: 'E',
		templateUrl: 'partials/tpl/login.tpl.html'
	}
}).directive('signupForm', function() {
	return{
		restrict: 'E',
		templateUrl: 'partials/tpl/signup.tpl.html'
	}
});