'use strict';

app.directive('appLoader', function() {
	return{
		restrict: 'E',
		templateUrl: 'partials/tpl/loader.tpl.html'
	}
});