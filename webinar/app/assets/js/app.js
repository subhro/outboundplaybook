'use strict';

var app = angular.module('MyApp', ['ngRoute']);

app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
	$routeProvider
	.when('/login', {
		url: '/login',
		templateUrl: 'partials/login.html',
		controller: 'loginCtrl'
	})
	.when('/chat', {
		url: '/chat',
		templateUrl: 'partials/chat.html',
		controller: 'chatCtrl'
	})
	.when('/idea', {
		url: 'idea',
		templateUrl: 'partials/idea.html',
		controller: 'ideaCtrl'
	})
	.when('/webinars', {
		url: '/webinars',
		templateUrl: 'partials/webinars.html',
		controller: 'webinarsCtrl'
	})
	.when('/webinar', {
		url: '/webinar',
		templateUrl: 'partials/webinar.html',
		controller: 'webinarCtrl'
	})
	.otherwise({
		redirectTo: '/login'
	});
}]);

app.run(['$rootScope', '$location', 'loginService', function($rootScope, $location, loginService) {
	var routespermission = ['/chat', '/idea', '/webinars', '/webinar']; // route that require login
	$rootScope.$on('$routeChangeStart', function() {
		if ( routespermission.indexOf($location.path()) != -1 ) {
			var connected = loginService.isLogged();
			connected.then(function(res) {
				if ( res.data.code != 0 ) {
					$location.path('/login');
				}
			});
		} else {
			var connected = loginService.isLogged();
			connected.then(function(res) {
				if ( res.data.code == 0 ) {
					$location.path('/webinars');
				}
			})
		}
	});
}]);