'use strict';

app.controller('ideaCtrl', ['$scope', 'loginService', function($scope, loginService) {
	$scope.loader = false;

	$scope.LogoutProcess = function() {
		loginService.logout();
	};

}]);