'use strict';

app.controller('webinarsCtrl', ['$scope', 'loginService', function($scope, loginService) {
	$scope.loader = false;

	$scope.LogoutProcess = function() {
		loginService.logout();
	};

}]);