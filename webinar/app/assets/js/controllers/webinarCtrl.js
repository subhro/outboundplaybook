'use strict';

app.controller('webinarCtrl', ['$scope', 'loginService', function($scope, loginService) {
	$scope.loader = false;

	$scope.LogoutProcess = function() {
		loginService.logout();
	};

}]);