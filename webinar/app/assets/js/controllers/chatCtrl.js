'use strict';

app.controller('chatCtrl', ['$scope', 'loginService', function($scope, loginService) {
	$scope.loader = false;

	$scope.LogoutProcess = function() {
		loginService.logout();
	};

}]);