'use strict';

app.controller('loginCtrl', ['$scope', 'loginService', function($scope, loginService) {
	$scope.loader = false;

	$scope.SigninProcess = function(signin) {
		$scope.loader = true;
		loginService.login(signin, $scope);
	};

	$scope.SignupProcess = function() {
		$scope.loader = true;
		loginService.login(signup, $scope);
	};

}]);