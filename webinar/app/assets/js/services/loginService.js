'use strict';

app.factory('loginService', ['$http', '$location', 'sessionService', function($http, $location, sessionService) {
	return{
		login: function(signin, scope) {
			var $promise = $http.post('php/?case=SigninProcess', signin);
			$promise.then(function(res) {
				if ( res.data.code == 0 ) {
					sessionService.set('UID', res.data.text);
					$location.path('/webinars');
					scope.loader = false;
				} else {
					scope.SigninError = res.data.text;
					$location.path('/login');
					scope.loader = false;
				}
			});
		},
		signup: function(signup, scope) {
			var $promise = $http.post('php/?case=SignupProcess', signup);
			$promise.then(function(res) {
				if ( res.data.code == 0 ) {
					scope.SignupError = res.data.text;
					scope.loader = false;
				} else {
					scope.SignupError = res.data.text;
					scope.loader = false;
				}
			});
		},
		logout: function() {
			sessionService.destroy('UID');
			$location.path('/login');
		},
		isLogged: function() {
			var $checkSessionServer = $http.post('php/?case=CheckSession');
			return $checkSessionServer;
		}
	}
}]);