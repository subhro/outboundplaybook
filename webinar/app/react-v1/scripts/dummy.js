var Dummy = {
    People: [
        {
            max : 40,
            min : 34
        }, {
            max : 55,
            min : 41
        }
    ],
    Comments: [
        {
            picture : 'assets/images/avatar5.png',
            name    : 'Josh Spoon',
            comment : 'Hey Soumitra, how are you doing?'
        }, {
            picture : 'assets/images/avatar.png',
            name    : 'Daven Michaels',
            comment : 'What’s written on the whiteboard?'
        }, {
            picture : 'assets/images/avatar5.png',
            name    : 'Beejel Parmar',
            comment : 'could you speak louder?'
        }, {
            picture : 'assets/images/avatar04.png',
            name    : 'Yogi Parmar',
            comment : 'Cold calling is dead- what do you think?'
        }, {
            picture : 'assets/images/avatar5.png',
            name    : 'Liz Hurley',
            comment : 'We are 100% outbound company?'
        }, {
            picture : 'assets/images/avatar.png',
            name    : 'Jane Austin',
            comment : 'What outbound prospecting tools do you recommend?'
        }, {
            picture : 'assets/images/avatar04.png',
            name    : 'Art Matuschat',
            comment : 'Is inbound cheaper than outbound?'
        }, {
            picture : 'assets/images/avatar.png',
            name    : 'Sudip Sen',
            comment : 'I am from India- we are selling to the USA'
        }, {
            picture : 'assets/images/avatar5.png',
            name    : 'Surajit Pramanik',
            comment : 'We are a web development company. How do I grow my business through outbound sales?'
        }, {
            picture : 'assets/images/avatar2.png',
            name    : 'Sonia',
            comment : 'Day I love outbound sales'
        }, {
            picture : 'assets/images/avatar3.png',
            name    : 'Taniya Sen',
            comment : 'What CRM do you use in your business?'
        }
    ],
    Notify: [
        {
            message : 'Presenter will ask a question',
            type    : 'info',
            audio   : 'assets/audios/sound.mp3'
        }, {
            message : 'ibero hendrerit erat, id',
            type    : 'danger',
            audio   : 'assets/audios/sound.mp3'
        }, {
            message : 'adipiscing elit. Suspendisse et turpis sed',
            type    : 'success',
            audio   : 'assets/audios/sound.mp3'
        }, {
            message : 'hendrerit erat, id blandit est lorem a tellus. Vestibulum tin',
            type    : 'warning',
            audio   : 'assets/audios/sound.mp3'
        }
    ],
    Polls: [
        {
            options  : ['YES', 'NO'],
            icons    : ['fa-thumbs-up', 'fa-thumbs-down'],
            colors   : ['#3399FF', '#FF0000', '#F9C840', '#19EA3C']
        }, {
            options  : ['Cold Emailing', 'Cold Calling', 'Social Media'],
            icons    : ['fa-envelope', 'fa-volume-control-phone', 'fa-comments-o'],
            colors   : ['#3399FF', '#FF0000', '#F9C840']
        }
    ],
    Pie: [
        {
            data : [
                ['Yes', 85.0], ['No',  15.0],
            ]
        }, {
            data : [
                ['Cold Emailing', 54.0],
                ['Cold Calling', 32.0],
                ['Social Media', 14.0]
            ]
        }
    ]
};