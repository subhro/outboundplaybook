var SigninForm = React.createClass({
    getInitialState: function() {
        this._handleChange = this._handleChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        return {
            email    : "",
            password : ""
        };
    },
    _handleChange: function(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    },
    _handleSubmit: function(e) {
        e.preventDefault();

        var email = this.state.email.trim();
        var password = this.state.password.trim();
        if ( !email || !password ) {
            return;
        }

        this.props.onSigninSubmit({email: email, password: password});
    },
    componentDidMount: function() {
        this.setState({
            email    : '',
            password : ''
        });
    },
    render: function() {
        return (
            <form method="post" autocomplete="false" onSubmit={this._handleSubmit}>
                <div className="form-group">
                    <input type="email" className="form-control" name="email" value={this.state.email} placeholder="Email" tabindex="1" onChange={this._handleChange} />
                </div>
                <div className="form-group">
                    <input type="password" className="form-control" name="password" placeholder="Password" tabindex="2" onChange={this._handleChange} />
                </div>
                <div className="form-group">
                    <button type="submit" name="signin" tabindex="3" className="btn btn-lg btn-danger">
                        <i className="glyphicon glyphicon-log-in"></i> Sign In
                    </button>
                </div>
                <p>{this.props.message}</p>
            </form>
        );
    }
});

var SignupForm = React.createClass({
    getInitialState: function() {
        this._handleChange = this._handleChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        return {
            fname: "",
            lname: "",
            email: "",
        };
    },
    _handleChange: function(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    },
    _handleSubmit: function(e) {
        e.preventDefault();

        var fname = this.state.fname.trim();
        var lname = this.state.lname.trim();
        var email = this.state.email.trim();
        if ( !fname || !lname || !email ) {
            return;
        }

        this.props.onSignupSubmit({fname: fname, lname: lname, email: email});
    },
    componentDidMount: function() {
        this.setState({
            fname: '',
            lname: '',
            email: '',
        });
    },
    render: function() {
        return (
            <form method="post" autocomplete="false" onSubmit={this._handleSubmit}>
                <div className="form-group">
                    <input type="text" className="form-control" name="fname" value={this.state.fname} placeholder="First Name" tabindex="1" onChange={this._handleChange} />
                </div>
                <div className="form-group">
                    <input type="text" className="form-control" name="lname" value={this.state.lname} placeholder="Last Name" tabindex="2" onChange={this._handleChange} />
                </div>
                <div className="form-group">
                    <input type="email" className="form-control" name="email" value={this.state.email} placeholder="Email" tabindex="3" onChange={this._handleChange} />
                </div>
                <div className="form-group">
                    <button type="submit" name="signup" tabindex="4" className="btn btn-lg btn-danger">
                        <i className="glyphicon glyphicon-log-in"></i> Sign Up
                    </button>
                </div>
                <p>{this.props.message}</p>
            </form>
        );
    }
});

var Login = React.createClass({
    getInitialState: function() {
        return {
            loading : true,
            signin_msg : '',
            signup_msg : '',
            data : []
        };
    },
    _handleSignInSubmit: function(signin) {
        this.setState({
            loading : true,
            signin_msg : '',
            signup_msg : '',
        });
        $.ajax({
            url      : this.props.signin_url,
            dataType : 'json',
            type     : 'POST',
            data     : signin,
            success  : function(res) {
                if ( res.code == 0 ) {
                    window.location = 'webinars.php';
                } else {
                    this.setState({
                        data : res,
                        signin_msg : res.text,
                        loading : false
                    });
                }
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.signin_url, status, err.toString());
            }.bind(this)
        });
    },
    _handleSignUpSubmit: function(signup) {
        $.ajax({
            url      : this.props.signup_url,
            dataType : 'json',
            type     : 'POST',
            data     : signup,
            success  : function(res) {
                this.setState({
                    data : res,
                    signup_msg : res.text,
                    loading : false
                });
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.signup_url, status, err.toString());
            }.bind(this)
        });
    },
    componentDidMount: function() {
        this.setState({
            loading : false
        });
    },
	render: function() {
        if ( this.state.loading ) {
            return (
                <div>
                    <Loader />
                </div>
            );
        } else {
    		return (
                <div className="login-body">
                    <article className="container-login">
                        <section>
                            <Tabs selected={0}>
                                <Pane label="Sign In">
                                    <div className="tabs-login">
                                        <p>Sign in to start your session</p>
                                        <SigninForm message={this.state.signin_msg} onSigninSubmit={this._handleSignInSubmit} />
                                    </div>
                                </Pane>
                                <Pane label="Sign Up">
                                    <div className="tabs-login">
                                        <p>Sign up to get access</p>                     
                                        <SignupForm message={this.state.signup_msg} onSignupSubmit={this._handleSignUpSubmit} />
                                    </div>
                                </Pane>
                            </Tabs>
                        </section>
                    </article>
                </div>
    		);
        }
	}
});

ReactDOM.render(
    <Login signin_url="php/?case=SigninProcess" signup_url="php/?case=SignupProcess" />, 
    document.body
);