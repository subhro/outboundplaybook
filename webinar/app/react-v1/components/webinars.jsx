var Webinars = React.createClass({
    getInitialState: function() {
        return {
            loading : true
        };
    },
    componentDidMount: function() {
        this.setState({
            loading : false
        });
    },
	render: function() {
        if ( this.state.loading ) {
            return (
                <div>
                    <Loader />
                </div>
            );
        } else {
    		return (
                <div>
                    <div className="chat-window bottom-zero">
                        <div className="panel">
                            <div className="panel-heading">
                                <span className="glyphicon glyphicon-expand"></span> My Webinars
                                <a href="logout.php" className="logout"><i className="glyphicon glyphicon-off"></i></a>
                            </div>
                            <div className="panel-body">
                                <ul className="list-group">
                                    <li className="list-group-item"><a href="webinar.php">Cras justo odio</a></li>
                                    <li className="list-group-item"><a href="webinar.php">Dapibus ac facilisis in</a></li>
                                    <li className="list-group-item"><a href="webinar.php">Morbi leo risus</a></li>
                                    <li className="list-group-item"><a href="webinar.php">Porta ac consectetur ac</a></li>
                                    <li className="list-group-item"><a href="webinar.php">Vestibulum at eros</a></li>
                                    <li className="list-group-item"><a href="webinar.php">Cras justo odio</a></li>
                                    <li className="list-group-item"><a href="webinar.php">Dapibus ac facilisis in</a></li>
                                    <li className="list-group-item"><a href="webinar.php">Morbi leo risus</a></li>
                                    <li className="list-group-item"><a href="webinar.php">Porta ac consectetur ac</a></li>
                                    <li className="list-group-item"><a href="webinar.php">Vestibulum at eros</a></li>
                                    <li className="list-group-item"><a href="webinar.php">Cras justo odio</a></li>
                                    <li className="list-group-item"><a href="webinar.php">Dapibus ac facilisis in</a></li>
                                    <li className="list-group-item"><a href="webinar.php">Morbi leo risus</a></li>
                                    <li className="list-group-item"><a href="webinar.php">Porta ac consectetur ac</a></li>
                                    <li className="list-group-item"><a href="webinar.php">Vestibulum at eros</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <BottomNav />
                </div>
    		);
        }
	}
});

ReactDOM.render(
    <Webinars />, 
    document.body
);