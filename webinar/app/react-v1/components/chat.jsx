var SendMSG = React.createClass({
	render: function() {
		return (
			<div className="msg_container base_sent">
                <div className="col-xs-10">
                    <div className="messages msg_sent">
                        <p>{this.props.msg}</p>
                        <time>{this.props.time}</time>
                    </div>
                </div>
                <div className="col-xs-2 avatar">
                    <img src={this.props.img} className="img-circle" />
                </div>
            </div>
		);
	}
});

var ReceiveMSG = React.createClass({
	render: function() {
		return (
			<div className="msg_container base_receive">
                <div className="col-xs-2 avatar">
                    <img src={this.props.img} className="img-circle" />
                </div>
                <div className="col-xs-10">
                    <div className="messages msg_receive">
                        <p>{this.props.msg}</p>
                        <time>{this.props.time}</time>
                    </div>
                </div>
            </div>
		);
	}
});

var ChatBox = React.createClass({
	getInitialState: function() {
		return {
			msg  	: "",
			time 	: "",
			img  	: "",
			loading : true
		};
	},
	componentDidMount: function() {
		$.ajax({
            url 	 : this.props.url,
            dataType : 'json',
            type 	 : 'POST',
            success  : function(res) {
                this.setState({
					msg  	: res.msg,
					time 	: res.time,
					img  	: res.img,
					loading : false,
				});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
	},
	_renderChatHeader: function(name, img) {
		return (
			<div className="panel-heading">
               	<img src={img} className="img-circle" /> {name}
               	<a href="logout.php" className="logout"><i className="glyphicon glyphicon-off"></i></a>
            </div>
		);
	},
	_renderChatFooter: function() {
		return (
			<div className="panel-footer">
                <div className="input-group">
                    <input type="text" className="form-control chat_input" placeholder="Write a message" />
                    <span className="input-group-btn">
	                    <button className="btn btn-danger">
	                    	<span className="glyphicon glyphicon-send"></span>
	                    </button>
                    </span>
                </div>
            </div>
		);
	},
	render: function() {
		if ( this.state.loading ) {
			return (
				<div>
					<Loader />
				</div>
			);
		} else {
		    return (
		    	<div>
					<div className="chat-window">
		    			<div className="panel">
		    				{this._renderChatHeader(this.props.username, this.props.userimg)}
				            <div className="panel-body">
								<SendMSG msg={this.state.msg} time={this.state.time} img={this.state.img} />
								<ReceiveMSG msg={this.state.msg} time={this.state.time} img={this.state.img} />
								<SendMSG msg={this.state.msg} time={this.state.time} img={this.state.img} />
								<ReceiveMSG msg={this.state.msg} time={this.state.time} img={this.state.img} />
								<SendMSG msg={this.state.msg} time={this.state.time} img={this.state.img} />
								<ReceiveMSG msg={this.state.msg} time={this.state.time} img={this.state.img} />
							</div>
							{this._renderChatFooter()}
						</div>
					</div>
					<BottomNav />
				</div>
		    );
		}
	}
});


ReactDOM.render(
	<ChatBox 
		username="Surajit Pramanik" 
		userimg="images/avatar.jpg" 
		url="php/?case=ChatData" />, 
	document.body
);