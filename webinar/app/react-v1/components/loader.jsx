var Loader = React.createClass({
	render: function() {
		return (
			<div className="loader">
	    		<div className="dot"></div>
				<div className="dot"></div>
				<div className="dot"></div>
				<div className="dot"></div>
				<div className="dot"></div>
				<div className="dot"></div>
				<div className="dot"></div>
				<div className="dot"></div>
			</div>
		);
	}
});