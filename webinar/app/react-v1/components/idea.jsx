var Idea = React.createClass({
    getInitialState: function() {
        return {
            loading : true
        };
    },
    componentDidMount: function() {
        this.setState({
            loading : false
        });
    },
	render: function() {
        if ( this.state.loading ) {
            return (
                <div>
                    <Loader />
                </div>
            );
        } else {
    		return (
                <div>
                    <div className="chat-window bottom-zero">
                        <div className="panel">
                            <div className="panel-heading">
                                <span className="glyphicon glyphicon-bullhorn"></span> Your Idea
                                <a href="logout.php" className="logout"><i className="glyphicon glyphicon-off"></i></a>
                            </div>
                            <div className="panel-body">
                                <div className="idea-form">
                                    <form>
                                        <div className="form-group">
                                            <label>Describe your ideas</label>
                                            <textarea rows="8" className="form-control"></textarea>
                                            <p className="help-block">Within 140 characters</p>
                                        </div>
                                        <div className="form-group">
                                            <button type="submit" className="btn btn-block btn-danger">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <BottomNav />
                </div>
    		);
        }
	}
});

ReactDOM.render(
    <Idea />, 
    document.body
);