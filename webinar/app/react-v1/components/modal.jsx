var Modal = React.createClass({
	render: function() {
		return (
			<div className="modal fade buyModal" id={this.props.id} tabindex="-1" role="dialog">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                            <h4 className="modal-title">Payment Information</h4>
                        </div>
                        <div className="modal-body">
                            <form method="post">
                                <div className="row">
                                    <div className="col-sm-12">
                                        <div className="form-group">
                                            <label for="card_no">Credit Card Number</label>
                                            <div className="row">
                                                <div className="col-sm-6">
                                                    <input type="text" className="card-number form-control" id="card_no" required data-msg-required="cannot be blank" />
                                                </div>
                                                <div className="col-sm-6">                          
                                                    <span className="cb-cards hidden-xs">                                        
                                                        <span className="visa"></span>
                                                        <span className="mastercard"></span>
                                                        <span className="american_express"></span>
                                                        <span className="discover"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <small for="card_no" className="text-danger"></small>
                                        </div>
                                    </div>                                                             
                                </div>
                                <div className="row">                
                                    <div className="col-sm-6">                                  
                                        <div className="form-group">
                                            <label for="expiry_month">Card Expiry</label>
                                            <div className="row">
                                                <div className="col-xs-6">
                                                    <select className="card-expiry-month form-control" id="expiry_month" required data-msg-required="empty">
                                                        <option selected>01</option>
                                                        <option>02</option>
                                                        <option>03</option>
                                                        <option>04</option>
                                                        <option>05</option>
                                                        <option>06</option>
                                                        <option>07</option>
                                                        <option>08</option>
                                                        <option>09</option>
                                                        <option>10</option>
                                                        <option>11</option>
                                                        <option>12</option>
                                                    </select>
                                                </div>
                                                <div className="col-xs-6">
                                                    <select className="card-expiry-year form-control" id="expiry_year" required data-msg-required="empty">
                                                        <option>2013</option>
                                                        <option>2014</option>
                                                        <option>2015</option>
                                                        <option>2016</option>
                                                        <option>2017</option>
                                                        <option>2018</option>
                                                        <option>2019</option>
                                                        <option selected="">2020</option>
                                                        <option>2021</option>
                                                        <option>2022</option>
                                                        <option>2023</option>
                                                    </select>
                                                </div>
                                            </div> 
                                            <small for="expiry_month" className="text-danger"></small>
                                            <small for="expiry_year" className="text-danger"></small>
                                        </div>                                       
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <label for="ccv">CCV</label>
                                            <div className="row">                                       
                                                <div className="col-xs-6">
                                                    <input type="text" className="card-cvc form-control" id="ccv" placeholder="CCV" required data-msg-required="empty" />
                                                </div>
                                                <div className="col-xs-6">                                              
                                                    <h6 className="cb-cvv"><small>(Last 3-4 digits)</small></h6>
                                                </div>
                                            </div>
                                            <small for="ccv" className="text-danger"></small>
                                        </div>
                                    </div>                                      
                                </div>                      
                                <div className="row">
                                    <div className="col-sm-12">
                                        <hr />
                                        <p>By clicking Subscribe, you agree to our privacy policy and terms of service.</p>
                                        <p><small className="text-danger hide">There were errors while submitting</small></p>
                                        <button type="submit" className="btn btn-primary btn-lg">Subscribe</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <span className="subscribe_process process hide">Processing&hellip;</span>
                                    </div>
                                </div>     
                            </form>
                        </div>
                    </div>
                </div>
            </div>
		);
	}
});