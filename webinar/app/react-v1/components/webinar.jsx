var videos = [  
    'videos/Webinar-test-Comp.mp4',
    'videos/Webinar-test-Comp.ogg',
    'videos/Webinar-test-Comp.webm'
];

var Webinar = React.createClass({
    getInitialState: function() {
        return {
            loading : true,
            autoplay: false,
            videoMp4Id : 0, videoOggId : 1, videoWebMId : 2,
            enterFullScreenBtnClass : 'glyphicon glyphicon-resize-full', exitFullScreenBtnClass : 'glyphicon glyphicon-resize-small hide',
        };
    },
    enterFullScreen: function() {
        var vidCon = this.refs.webinarContainer;
        if (vidCon.requestFullscreen) {
            vidCon.requestFullscreen();
        } else if (vidCon.mozRequestFullScreen) {
            vidCon.mozRequestFullScreen();
        } else if (vidCon.webkitRequestFullscreen) {
            vidCon.webkitRequestFullscreen();
        }
        this.setState({
            enterFullScreenBtnClass : 'glyphicon glyphicon-resize-full hide',
            exitFullScreenBtnClass : 'glyphicon glyphicon-resize-small'
        });
    },
    exitFullScreen: function() {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
        this.setState({
            enterFullScreenBtnClass : 'glyphicon glyphicon-resize-full',
            exitFullScreenBtnClass : 'glyphicon glyphicon-resize-small hide'
        });
    },
    componentDidMount: function() {
        this.setState({
            loading : false,
            autoplay: true
        });
    },
	render: function() {
        if ( this.state.loading ) {
            return (
                <div>
                    <Loader />
                </div>
            );
        } else {
    		return (
                <div>
                    <div className="chat-window bottom-zero">
                        <div className="panel">
                            <div className="panel-heading">
                                <span className="glyphicon glyphicon-expand"></span> Webinar
                                <a href="logout.php" className="logout"><i className="glyphicon glyphicon-off"></i></a>
                            </div>
                            <div className="panel-body no-padding">
                                <div className="video-wrap" ref="webinarContainer">
                                    <video width="100%" height="100%" autoPlay={this.state.autoplay} ref="webinarPlayer">
                                        <source src={videos[this.state.videoMp4Id]} type="video/mp4" />
                                        <source src={videos[this.state.videoOggId]} type="video/ogg" />
                                        <source src={videos[this.state.videoWebMId]} type="video/webm" />
                                    </video>

                                    <div className="vmessages"></div>
                                    <div className="live-icon">
                                        <span className="blink">LIVE</span>
                                    </div>
                                    <div className="comment-count">
                                        <i className="glyphicon glyphicon-user" aria-hidden="true"></i> <span id="users">34</span>
                                    </div>
                                    <div className="fullscreen">
                                        <i onClick={this.enterFullScreen} className={this.state.enterFullScreenBtnClass}></i>
                                        <i onClick={this.exitFullScreen} className={this.state.exitFullScreenBtnClass}></i>
                                    </div>

                                    <div className="flying-icons"></div>
                                    <div className="charts">
                                        <div id="pie3d"></div>
                                        <div id="question"></div>
                                    </div>
                                    <div className="buy-button hide">
                                        <button className="btn btn-sm btn-success" data-toggle="modal" data-target="#buyModal">
                                            BUY NOW!
                                        </button>
                                        <Modal id="buyModal" />
                                    </div>
                                    <div className="comments">
                                        <div className="comment-inner">
                                            <div className="nano">
                                                <ul className="list-unstyled nano-content" id="comment-list"></ul>
                                            </div>
                                        </div>
                                        <div className="comment-input">
                                            <input type="text" className="form-control" id="comment" placeholder="Write Comments" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <BottomNav />
                </div>
    		);
        }
	}
});

ReactDOM.render(
    <Webinar />, 
    document.body
);