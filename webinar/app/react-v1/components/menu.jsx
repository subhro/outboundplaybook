var BottomNav = React.createClass({
	render: function() {
		return (
			<div className="btn-footer">
				<a href="chat.php"><button className="bg_none"><i className="glyphicon glyphicon-film"></i></button></a>
				<a href="idea.php"><button className="bg_none"><i className="glyphicon glyphicon-camera"></i></button></a>
			    <a href="webinars.php"><button className="bg_none"><i className="glyphicon glyphicon-paperclip"></i></button></a>
				<a href="webinar.php"><button className="bg_none pull-right"><i className="glyphicon glyphicon-thumbs-up"></i></button></a>
			</div>
		);
	}
});