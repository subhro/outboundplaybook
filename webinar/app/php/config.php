<?php 

// ** MySQL Settings - You can get this info from your web host ** //
define('DB_NAME', 		'outboundplaybook');//
define('DB_USER', 		'root');//
define('DB_PASSWORD', 	'');//
define('DB_HOST', 		'localhost');//
define('DB_CHARSET', 	'utf8');
define('DB_COLLATE', 	'utf8_unicode_ci');
define('DB_PREFIX', 	'wp_');

// ** Database Class API  ** //
include(dirname(__FILE__) . "/db.class.php");
$db = new DB(DB_NAME, DB_HOST, DB_USER, DB_PASSWORD);

// ** Application BASE URL ** //
define('BASE_URL', 'http://localhost/outboundplaybook/webinar/app/');

// ** Application Environment ** //
define('DEBUG', false);

if ( DEBUG == true ) {
	ini_set( "display_errors", 1 );
} else {
	ini_set( "display_errors", 0 );
}