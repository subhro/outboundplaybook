<?php 
include( dirname(__FILE__) . '/config.php' );

$case = stripslashes($_GET['case']);
switch ( $case ) {
	case 'SigninProcess':
		SigninProcess();
		break;
	case 'SignupProcess':
		SignupProcess();
		break;
	case 'DestroySession':
		DestroySession();
		break;
	case 'CheckSession':
		CheckSession();
		break;
	default:
		echo '404! Page Not Found';
		break;
}

function SigninProcess() {
	$res = array();

	$signin = json_decode(file_get_contents('php://input')); // get signin data from json headers
	if ( !empty($signin->email) && !empty($signin->password) ) {
		$email 	  = addslashes($signin->email);
		$password = addslashes($signin->password);
		$query = mysql_query("SELECT * FROM `" . DB_PREFIX . "webinar` WHERE `Cust_Email` = '$email' AND `Cust_App_Password` = '$password'");
		if ( $query ) {
			if ( mysql_num_rows($query) == 1 ) {
				$data = mysql_fetch_assoc($query);
				session_start();
				$_SESSION['UID'] = $data['Cust_ID'];
				$res['code'] = 0;
				$res['text'] = $_SESSION['UID'];
			} else {
				$res['code'] = 1;
				$res['text'] = 'Wrong Email or Password.';
			}
		} else {
			$res['code'] = 2;
			$res['text'] = 'Something went wrong, try again.';
		}
	} else {
		$res['code'] = 3;
		$res['text'] = 'Email & Password required.';
	}

	echo json_encode($res);
}

function SignupProcess() {
	$res = array();

	$signup = json_decode(file_get_contents('php://input')); // get signin data from json headers
	if ( !empty($signup->fname) && !empty($signup->lname) && !empty($signup->email) ) {
		$fname = addslashes($signup->fname);
		$lname = addslashes($signup->lname);
		$email = addslashes($signup->email);
		$query = mysql_query("INSERT INTO `" . DB_PREFIX . "webinar`(`Cust_FirstName`, `Cust_LastName`, `Cust_Email`) VALUES('$fname', '$lname', '$email')");
		if ( $query ) {
			$res['code'] = 0;
			$res['text'] = 'Successfully registerd.';
		} else {
			$res['code'] = 1;
			$res['text'] = 'Something went wrong, try again.';
		}
	} else {
		$res['code'] = 2;
		$res['text'] = 'All Fields are required.';
	}

	echo json_encode($res);
}

function DestroySession() {
	session_id('UID');
	session_start();
	session_destroy();
	session_commit();
}

function CheckSession() {
	$res = array();
	session_start();
	if ( isset($_SESSION['UID']) ) {
		$res['code'] = 0;
		$res['text'] = 'authentified';
	}

	echo json_encode($res);
}