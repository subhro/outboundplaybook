<?php 
include_once( dirname( dirname( __FILE__ ) ) . "/config/google-api.php" );
include_once( dirname( dirname( __FILE__ ) ) . "/config/mail-process.php" );
require( dirname( dirname( dirname( __FILE__ ) ) ) . '/wp-load.php' );
global $wpdb;

$webinarSQL = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "webinar_email_listing WHERE Listing_Email_Status = 'NO' AND Listing_Email_Time LIKE '".date("Y-m-d")."%'" );

if( $webinarSQL ) {
	$num_rows = $wpdb->get_var( "SELECT COUNT(*) FROM " . $wpdb->prefix . "webinar_email_listing WHERE Listing_Email_Status = 'NO' AND Listing_Email_Time LIKE '".date("Y-m-d")."%'" );
	
	if( $num_rows > 0 ) {
		foreach( $webinarSQL as $webinar ) {
			$webinarUserID 		= $webinar->Listing_Cust_ID;
			$UserData 			= $wpdb->get_row( "SELECT * FROM " . $wpdb->prefix . "webinar WHERE Cust_ID = ".$webinarUserID );
			$webinarDateTime 	= $webinar->Listing_Email_Time;
			$webinarFirstname	= $UserData->Cust_FirstName;
			$webinarLastname	= $UserData->Cust_LastName;
			$webinarEmail		= $UserData->Cust_Email;
			$webinarFullName	= $webinarFirstname . " " . $webinarLastname;
			$webinarEmailStatus = $webinar->Listing_Email_Status;

			// Webinar Email Cron & Update Query
			if( $webinarEmailStatus == 'NO' && $webinarDateTime >= date("Y-m-d H:i:s") ) {
				$updateSQL = $wpdb->query( "UPDATE " . $wpdb->prefix ."webinar_email_listing SET Listing_Email_Status = 'YES' WHERE Listing_Email_Time LIKE '".date("Y-m-d")."%' AND Listing_Cust_ID = ".$webinarUserID );
				if( $updateSQL ) {
					$to 		= $webinarEmail;
					$subject 	= $webinar->Listing_Email_Subject;
					$body 		= $webinar->Listing_Email;
					Send_Mail( $subject, $body, $webinarFullName, $to );
					echo "Email just send.<br>";
				} else {
					echo 'Error Sending Email';
				}
			} else {
				echo "Email already Send before.<br>";
			}
		}
	} else {
		echo "No Data found";
	}
} else {
	echo "DB ERROR";
}
?>