<?php 
include_once( dirname( dirname( __FILE__ ) ) . "/config/google-api.php" );
include_once( dirname( dirname( __FILE__ ) ) . "/config/mail-process.php" );
require( dirname( dirname( dirname( __FILE__ ) ) ) . '/wp-load.php' );
global $wpdb;

$webinarSQL = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "webinar WHERE Cust_Phone = ''" );

if( $webinarSQL ) {
	$num_rows = $wpdb->get_var( "SELECT COUNT(*) FROM " . $wpdb->prefix . "webinar WHERE Cust_Phone = ''" );	
	
	if( $num_rows > 0 ) {
		foreach( $webinarSQL as $webinar ) {
			$webinarUserID 		= $webinar->Cust_ID;
			$UserData 			= $wpdb->get_row( "SELECT * FROM " . $wpdb->prefix . "webinar WHERE Cust_ID = ".$webinarUserID );		
			$webinarDateTime 	= $UserData->Cust_WebinarDate;
			$webinarFirstname	= $UserData->Cust_FirstName;
			$webinarLastname	= $UserData->Cust_LastName;
			$webinarEmail		= $UserData->Cust_Email;
			$webinarPhone		= $UserData->Cust_Phone;
			$webinarUniqueID 	= $UserData->Cust_Unique_ID;
			$webinarFullName	= $webinarFirstname . " " . $webinarLastname;
			$phoneUpdateLink 	= site_url("/webinar/addPhone/?_web_token=".$webinarUniqueID);

			// Phone Email Cron
			if( $webinarPhone == '' && date("Y-m-d H:i:s") <= $webinarDateTime ) {
				$to 		= $webinarEmail;
				$subject 	= 'Please Update your Phone';
				$body 		= 'Please Update your Phone number from this link: '.$phoneUpdateLink;
				Send_Mail( $subject, $body, $webinarFullName, $to );
				echo "Email just send.<br>";
			} else {
				echo 'Error Sending Email';
			}			
		}
	} else {
		echo "No Data found";
	}
} else {
	echo "DB ERROR";
}
?>