<?php 
include_once( dirname( dirname( __FILE__ ) ) . "/config/google-api.php" );
include_once( dirname( dirname( __FILE__ ) ) . "/config/mail-process.php" );
require( dirname( dirname( dirname( __FILE__ ) ) ) . '/wp-load.php' );
global $wpdb;

$date_time_array = array();
$dateTimeSQL = $wpdb->get_results( "SELECT * FROM `" . $wpdb->prefix . "webinar_schedule` WHERE `Schedule_Date_Type` = 'Live' AND `Schedule_Date_Status` = 'NO' OR `Schedule_Date_Status` = 'RUNNING'" );
if( $dateTimeSQL ) {
	if( count( $dateTimeSQL ) > 0 ) {
		foreach( $dateTimeSQL as $dateTime ) {
			$date_time_array[] = date( "Y-m-d", strtotime($dateTime->Schedule_DateTime) );
		}
		$current_date = date( "Y-m-d" );
		$current_time = date( "Y-m-d H:i:s" );
			if ( in_array( $current_date, $date_time_array ) ) {
				$dateSQL = $wpdb->get_row( "SELECT * FROM `" . $wpdb->prefix . "webinar_schedule` WHERE `Schedule_Date_Type` = 'Live' AND DATE(`Schedule_DateTime`) = '".$current_date."'" );
				if( count( $dateSQL ) > 0) {
					$webinar_date = $dateSQL;
					$scheduleID = $webinar_date->Schedule_ID;
					$currentDate = strtotime( $webinar_date->Schedule_DateTime );
					$fDate = $currentDate + ( 60*3.5 );
					$fetureDate = date( "Y-m-d H:i:s", $fDate );
					echo "Webinar Start Time : ".$webinar_date->Schedule_DateTime."<br>";
					echo "Current Time : ".$current_time."<br>";
					echo "Webinar End Time : ".$fetureDate."<br>";
					
					if( $current_time < $webinar_date->Schedule_DateTime ) {
						echo "Webinar is not started yet.";
					} else if( $webinar_date->Schedule_DateTime <= $current_time && $current_time < $fetureDate ) {
						$statusSQL = $wpdb->get_row( "SELECT * FROM `" . $wpdb->prefix . "webinar_Status` WHERE `Webinar_Live_Date` = '".$current_date ."'" );
						if ( count( $statusSQL ) == 0 ) {
							$runningSql = $wpdb->query( "INSERT INTO `" . $wpdb->prefix . "webinar_Status` (`Webinar_Live_Date`, `Webinar_Current_Time`, `Webinar_End_Time`) VALUES ('".$current_date."', '".$current_time."', '".$fetureDate."')" );
						} else {
							$runningSql = $wpdb->query( "UPDATE `" . $wpdb->prefix . "webinar_Status` SET `Webinar_Current_Time`='".$current_time."',`Webinar_End_Time`='".$fetureDate."' WHERE `Webinar_Live_Date` = '".$current_date."'" );
						}
						echo "Webinar is Running.";
					} else if( $current_time <= $fetureDate ) {
						echo "Webinar is Complete";
						$completeSQL = $wpdb->query( "UPDATE `wp_webinar_schedule` SET `Schedule_Date_Status`= 'COMPLETE' WHERE `Schedule_ID` = ".$scheduleID );
					}
				} else {
					echo "Match not found";
				}					
			} else {
				echo "No Webinar in this date";
			}	
	} else {
		echo "No Date found";
	}
} else {
	echo "DB Error";
}
?>