<?php 
include_once( dirname( dirname( __FILE__ ) ) . "/config/google-api.php" );
include_once( dirname( dirname( __FILE__ ) ) . "/config/mail-process.php" );
require( dirname( dirname( dirname( __FILE__ ) ) ) . '/wp-load.php' );
global $wpdb;

$webinarSQL = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "webinar WHERE Cust_WebinarDate >= '".date('Y-m-d H:i:s')."' AND Cust_Webinar_Status = 'Unseen'" );

if( $webinarSQL ) {
	$num_rows = $wpdb->get_var( "SELECT COUNT(*) FROM " . $wpdb->prefix . "webinar WHERE Cust_WebinarDate >= '".date('Y-m-d H:i:s')."' AND Cust_Webinar_Status = 'Unseen'" );	
	
	if( $num_rows > 0 ) {
		foreach( $CheckUser as $UserData ) {
			$webinarUserID 		= $UserData->Cust_ID;
			$webinarFirstname	= $UserData->Cust_FirstName;
			$webinarLastname	= $UserData->Cust_LastName;
			$webinarEmail		= $UserData->Cust_Email;
			$webinarFullName	= $webinarFirstname . " " . $webinarLastname;

			$webinar 			= $wpdb->get_row( "SELECT * FROM " . $wpdb->prefix . "webinar_missed_listing WHERE Missed_Cust_ID = ".$webinarUserID );
			$webinarDateTime 	= $webinar->Missed_Email_Time;
			$webinarEmailStatus = $webinar->Missed_Email_Status;		

			if( $webinarEmailStatus == 'NO' && $webinarDateTime >= date("Y-m-d H:i:s") ) {
				$updateSQL = $wpdb->query( "UPDATE " . $wpdb->prefix ."webinar_missed_listing SET Missed_Email_Status = 'YES' WHERE Missed_Email_Time LIKE '".date("Y-m-d")."%' AND Missed_Cust_ID = ".$webinarUserID );
				if( $updateSQL ) {
					$to 		= $webinarEmail;
					$subject 	= $webinar->Missed_Email_Subject;
					$body 		= $webinar->Missed_Email;
					Send_Mail( $subject, $body, $webinarFullName, $to );
					echo "Email just send.<br>";
				} else {
					echo 'Error Sending Email';
				}
			} else {
				echo "Email already Send before.<br>";
			}
		}
	} else {
		echo "No Data found";
	}
} else {
	echo "DB ERROR";
}
?>