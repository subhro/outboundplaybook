<?php
require(dirname(dirname(dirname(__FILE__))) . '/wp-load.php');
include_once(dirname(dirname(__FILE__)) . "/config/mail-process.php");

require_once(dirname(dirname(__FILE__)) . "/config/twilio/Twilio/autoload.php");
use Twilio\Rest\Client;

$case = isset($_REQUEST['case']) ? $_REQUEST['case'] : '';
switch($case) {
	case 'WebinarSignupForm':
		WebinarSignupForm();
		break;
	case 'CheckUserDataExist':
		CheckUserDataExist();
		break;
	case 'WebinarVideoStatus':
		WebinarVideoStatus();
		break;
	case 'AddWebinarPhone':
		AddWebinarPhone();
		break;
	case 'ScheduleWebinar':
		ScheduleWebinar();
		break;
	case 'WebinarUpdateForm':
		WebinarUpdateForm();
		break;
	case 'GetCurrentPHPDateTime':
		GetCurrentPHPDateTime();
		break;
	case 'BuyNowWebinar':
		BuyNowWebinar();
		break;
	case 'LiveWebinarTime':
		LiveWebinarTime();
		break;
	case 'MissedWebinar':
		MissedWebinar();
		break;
	default:
		echo '404 not found!';
		break;
}

function WebinarSignupForm() {
	global $wpdb;
	if ( !session_id() ) session_start();

	$result 	 = array();
	$first_name  = addslashes($_POST['first_name']);
	$last_name 	 = addslashes($_POST['last_name']);
	$email_id 	 = addslashes($_POST['email_id']);
	$current_url = addslashes($_POST['current_url']);
	if ( !empty($first_name) && !empty($last_name) && !empty($email_id) ) {
		$CheckUserExist = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "webinar` WHERE `Cust_Email` = '$email_id'");
		if ( $CheckUserExist->num_rows == 0 ) {
			$WebinarSession = array(
				'Webinar_FirstName' 	=> $first_name,
				'Webinar_LastName' 		=> $last_name,
				'Webinar_EmailID' 		=> $email_id,
				'Webinar_Callback_URL'	=> $current_url
			);
			$_SESSION['_webinar_data'] = $WebinarSession;
			$result['code'] = 0;
			$result['result'] = site_url('/webinar/schedule/');
		} else {
			$result['code'] = 1;
			$result['result'] = '<p class="text-danger text-center">Email aleady exist.</p>';
		}
	} else {
		$result['code'] = 2;
		$result['result'] = '<p class="text-danger text-center">Please fill the details before submit!</p>';
	}

	echo json_encode($result);
}

function CheckUserDataExist() {
	global $wpdb;
	
	$valid 	 = true;
	$users 	 = array();
	$UserSQL = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "webinar");
	if ( $UserSQL ) {		
		$i = 0;
		foreach ($UserSQL as $key => $value) {
		 	$users[$i]['Cust_Email'] = $value->Cust_Email;
		 	$i++;
		}
	}
	foreach ( $users as $user ) {
		if ( isset($_POST['email_id']) ) {
			if ( $_POST['email_id'] == $user['Cust_Email'] ) {
				$valid   = false;
				break;
			}
		}  
	}
	echo json_encode(
		$valid ? array('valid' => $valid) : array('valid' => $valid)
	);
}

function WebinarVideoStatus() {
	global $wpdb;

	$UserUniqueID 	= addslashes($_POST['UserUniqueID']);
	$CurrentTime 	= addslashes($_POST['CurrentTime']);
	$CheckStatus 	= $wpdb->get_row("SELECT * FROM `" . $wpdb->prefix . "webinar` WHERE `Cust_Unique_ID` = '".$UserUniqueID."'");
	$Status 		= $CheckStatus->Cust_Webinar_Status;
	if( $Status == 'Complete' ) {
		$WebinarStatus  = 'Complete';
	} else {
		$WebinarStatus  = 'Seen';
	}
	
	if( !empty($UserUniqueID) && !empty($CurrentTime) ) {
		$wpdb->update( 
			$wpdb->prefix . 'webinar',
			array(
				'Cust_Webinar_CompleteTime'	=> $CurrentTime,
				'Cust_Webinar_Status' 		=> $WebinarStatus,
			),
			array(
				'Cust_Unique_ID' => $UserUniqueID
			)
		);
		echo 1;
	} else {
		echo 0;
	}
}

function AddWebinarPhone() {
	global $wpdb;

	$PhoneNumer = addslashes($_POST['Phone']);	
	$EmailID 	= addslashes($_POST['Email']);
	$CheckUserExist = $wpdb->get_row("SELECT * FROM `" . $wpdb->prefix . "webinar` WHERE `Cust_Email` = '$EmailID'");
	if ( count($CheckUserExist) > 0 ) {
		$applink = 'https://goo.gl/rtWwZL';
		$password = $CheckUserExist->Cust_App_Password;

		if ( !empty($PhoneNumer) && !empty($EmailID) ) {
			$wpdb->update( 
				$wpdb->prefix . 'webinar',
				array(
					'Cust_Phone' => $PhoneNumer,
				),
				array(
					'Cust_Email' => $EmailID
				)
			);	

			$sid = 'AC04f403016bdde124f2ce58fb6955d487';
			$token = 'fa8526a483260cb04a7da71aa5dfd756';
			$client = new Client($sid, $token);

			$people = array(
			    $PhoneNumer => $CheckUserExist->Cust_FirstName . ' ' . $CheckUserExist->Cust_LastName
			);
			foreach ($people as $number => $name) {
			   	$sms = $client->account->messages->create($number, array(
			   		'from' => "+14048651097",
			   		'body' => "Download free app from $applink. Use this login- Email: $EmailID Password: $password"
			   	));
			}

			$webinarEmail  = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "webinar_email" );
			foreach( $webinarEmail as $webinar ) {
				$Email_AppLink_Subject	= $webinar->Email_AppLink_Subject;
				$Email_AppLink			= $webinar->Email_AppLink;		
			}
			$to    	 = $EmailID;
            $subject = $Email_AppLink_Subject;
            $body 	 = strtr( 
				$Email_AppLink, 
				array(
					'#FirstName#' 	=> $FirstName,
					'#Email#' 		=> $EmailID,
					'#AppPassword#' => $password,
					'#AndroidLink#' => $applink,
					'#MacLink#'		=> $applink,
				)
			);
            Send_Mail( $subject, $body, $FirstName.' '.$LastName, $to );

			echo 1;
		} else {
			echo 0;
		}
	} else {
		echo 0;
	}
}

function ScheduleWebinar() {
	global $wpdb;
	if ( !session_id() ) session_start();

	$Live 	= $_POST['LiveDate'];
	$Encore = $_POST['EncoreDate'];
    $GetLiveDate   = $wpdb->get_results("SELECT Schedule_DateTime FROM " . $wpdb->prefix . "webinar_schedule WHERE Schedule_ID = " . $Live);
    $GetEncoreDate = $wpdb->get_results("SELECT Schedule_DateTime FROM " . $wpdb->prefix . "webinar_schedule WHERE Schedule_ID = " . $Encore);
	$FirstName     = $_SESSION['_webinar_data']['Webinar_FirstName'];
    $LastName      = $_SESSION['_webinar_data']['Webinar_LastName'];
    $EmailID       = $_SESSION['_webinar_data']['Webinar_EmailID'];
    $FullName      = $FirstName . " " . $LastName;
    $Characters    = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $UserUniqueID  = substr( str_shuffle( $Characters ), 0, 7 );
    $appPassword   = rand(1000, 9999);

    $SignupDate    = date('Y-m-d H:i:s');
    $WebinarDate   = $GetLiveDate[0]->Schedule_DateTime;
    $EncoreDate	   = $GetEncoreDate[0]->Schedule_DateTime;
    $dateDiff      = strtotime($WebinarDate) - strtotime($SignupDate);
    $daysLive      = ($dateDiff/(60*60*24))%365;
	$hoursLive     = ($dateDiff/(60*60))%24;
	$minutesLive   = ($dateDiff/60)%60;
	$secondsLive   = ($dateDiff%60);	
    $WebinarLength = 45;

    $webinarEmail  = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "webinar_email" );
	foreach( $webinarEmail as $webinar ) {
		$Email_ScheduleDate_Subject	= $webinar->Email_ScheduleDate_Subject;
		$Email_ScheduleDate			= $webinar->Email_ScheduleDate;		
	}
	
	$CheckUserSchedule = $wpdb->get_row( "SELECT * FROM " . $wpdb->prefix . "webinar WHERE Cust_Email = '".$EmailID."'" );
	if( empty($CheckUserSchedule) ) {
		$WebinarData = array( 
            'Cust_FirstName'  		=> $FirstName, 
            'Cust_LastName'   		=> $LastName,
            'Cust_Email'      		=> $EmailID,
            'Cust_Unique_ID'  		=> $UserUniqueID,
            'Cust_App_Password'		=> $appPassword,
            'Cust_SignupDate' 		=> $SignupDate,
            'Cust_WebinarDate'		=> $WebinarDate,
            'Cust_EncoreDate'		=> $EncoreDate,
            'Cust_WebinarLength'	=> $WebinarLength

        );
        $UserInsert = $wpdb->insert( $wpdb->prefix . 'webinar', $WebinarData );
		$Cust_ID 	= $wpdb->insert_id;
		
		if($daysLive >= 7) {

			$listingSQL = $wpdb->query("INSERT INTO " . $wpdb->prefix . "webinar_email_listing 
				(`Listing_Cust_ID`, `Listing_Email_Time`, `Listing_Email_Subject`, `Listing_Email`) VALUES 
				(".$Cust_ID.", '".date('Y-m-d H:i:s', strtotime($SignupDate . " +1 days"))."', 'Email_Drift1_Subject', 'Email_Drift1'),
				(".$Cust_ID.", '".date('Y-m-d H:i:s', strtotime($SignupDate . " +3 days"))."', 'Email_Drift2_Subject', 'Email_Drift2'),
				(".$Cust_ID.", '".date('Y-m-d H:i:s', strtotime($SignupDate . " +6 days"))."', 'Email_Drift3_Subject', 'Email_Drift3'),
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (2*60*60))."', 'twohour_Email_Subject', 'twohour_Email'),
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (30*60))."', 'thirtyMinute_Email_Subject', 'thirtyMinute_Email'),
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (5*60))."', 'fiveMinute_Email_Subject', 'fiveMinute_Email')");
			$missedSQL = $wpdb->query("INSERT INTO " . $wpdb->prefix . "webinar_missed_listing 
				(`Missed_Cust_ID`, `Missed_Email_Time`, `Missed_Email_Subject`, `Missed_Email`) VALUES 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) + (60*60))."', 'initial_Missing_Email_subject', 'initial_Missing_Email')");
			
		} else if($daysLive < 7 && $daysLive == 6) {

			$listingSQL = $wpdb->query("INSERT INTO " . $wpdb->prefix . "webinar_email_listing 
				(`Listing_Cust_ID`, `Listing_Email_Time`, `Listing_Email_Subject`, `Listing_Email`) VALUES 
				(".$Cust_ID.", '".date('Y-m-d H:i:s', strtotime($SignupDate . '+1 days'))."', 'Email_Drift1_Subject', 'Email_Drift1'), 
				(".$Cust_ID.", '".date('Y-m-d H:i:s', strtotime($SignupDate . '+3 days'))."', 'Email_Drift2_Subject', 'Email_Drift2'), 
				(".$Cust_ID.", '".date('Y-m-d H:i:s', strtotime($SignupDate . '+5 days'))."', 'Email_Drift3_Subject', 'Email_Drift3'), 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (2*60*60))."', 'twohour_Email_Subject', 'twohour_Email'),
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (30*60))."', 'thirtyMinute_Email_Subject', 'thirtyMinute_Email'), 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (5*60))."', 'fiveMinute_Email_Subject', 'fiveMinute_Email')");
			$missedSQL = $wpdb->query("INSERT INTO " . $wpdb->prefix . "webinar_missed_listing 
				(`Missed_Cust_ID`, `Missed_Email_Time`, `Missed_Email_Subject`, `Missed_Email`) VALUES 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) + (60*60))."', 'initial_Missing_Email_subject', 'initial_Missing_Email')");
			
		} else if($daysLive < 6 && $daysLive == 5) {
			
			$listingSQL = $wpdb->query("INSERT INTO " . $wpdb->prefix . "webinar_email_listing 
				(`Listing_Cust_ID`, `Listing_Email_Time`, `Listing_Email_Subject`, `Listing_Email`) VALUES 
				(".$Cust_ID.", '".date('Y-m-d H:i:s', strtotime($SignupDate . '+1 days'))."', 'Email_Drift1_Subject', 'Email_Drift1'), 
				(".$Cust_ID.", '".date('Y-m-d H:i:s', strtotime($SignupDate . '+2 days'))."', 'Email_Drift2_Subject', 'Email_Drift2'), 
				(".$Cust_ID.", '".date('Y-m-d H:i:s', strtotime($SignupDate . '+4 days'))."', 'Email_Drift3_Subject', 'Email_Drift3'), 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (2*60*60))."', 'twohour_Email_Subject', 'twohour_Email'), 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (30*60))."', 'thirtyMinute_Email_Subject', 'thirtyMinute_Email'), 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (5*60))."', 'fiveMinute_Email_Subject', 'fiveMinute_Email')");
			$missedSQL = $wpdb->query("INSERT INTO " . $wpdb->prefix . "webinar_missed_listing 
				(`Missed_Cust_ID`, `Missed_Email_Time`, `Missed_Email_Subject`, `Missed_Email`) VALUES 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) + (60*60))."', 'initial_Missing_Email_subject', 'initial_Missing_Email')");
			
		} else if($daysLive < 5 && $daysLive == 4) {

			$listingSQL = $wpdb->query("INSERT INTO " . $wpdb->prefix . "webinar_email_listing 
				(`Listing_Cust_ID`, `Listing_Email_Time`, `Listing_Email_Subject`, `Listing_Email`) VALUES 
				(".$Cust_ID.", '".date('Y-m-d H:i:s', strtotime($SignupDate . '+1 days'))."', 'Email_Drift1_Subject', 'Email_Drift1'), 
				(".$Cust_ID.", '".date('Y-m-d H:i:s', strtotime($SignupDate . '+2 days'))."', 'Email_Drift2_Subject', 'Email_Drift2'), 
				(".$Cust_ID.", '".date('Y-m-d H:i:s', strtotime($SignupDate . '+3 days'))."', 'Email_Drift3_Subject', 'Email_Drift3'), 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (2*60*60))."', 'twohour_Email_Subject', 'twohour_Email'), 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (30*60))."', 'thirtyMinute_Email_Subject', 'thirtyMinute_Email'), 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (5*60))."', 'fiveMinute_Email_Subject', 'fiveMinute_Email')");
			$missedSQL = $wpdb->query("INSERT INTO " . $wpdb->prefix . "webinar_missed_listing 
				(`Missed_Cust_ID`, `Missed_Email_Time`, `Missed_Email_Subject`, `Missed_Email`) VALUES 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) + (60*60))."', 'initial_Missing_Email_subject', 'initial_Missing_Email')");
			
		} else if($daysLive < 4 && $daysLive == 3) {

			$listingSQL = $wpdb->query("INSERT INTO " . $wpdb->prefix . "webinar_email_listing 
				(`Listing_Cust_ID`, `Listing_Email_Time`, `Listing_Email_Subject`, `Listing_Email`) VALUES 
				(".$Cust_ID.", '".date('Y-m-d H:i:s', strtotime($SignupDate . '+1 days'))."', 'Email_Drift1_Subject', 'Email_Drift1'), 
				(".$Cust_ID.", '".date('Y-m-d H:i:s', strtotime($SignupDate . '+2 days'))."', 'Email_Drift2_Subject', 'Email_Drift2'), 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (2*60*60))."', 'twohour_Email_Subject', 'twohour_Email'), 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (30*60))."', 'thirtyMinute_Email_Subject', 'thirtyMinute_Email'), 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (5*60))."', 'fiveMinute_Email_Subject', 'fiveMinute_Email')");
			$missedSQL = $wpdb->query("INSERT INTO " . $wpdb->prefix . "webinar_missed_listing 
				(`Missed_Cust_ID`, `Missed_Email_Time`, `Missed_Email_Subject`, `Missed_Email`) VALUES 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) + (60*60))."', 'initial_Missing_Email_subject', 'initial_Missing_Email')");
			
		} else if($daysLive < 3 && $daysLive == 2) {

			$listingSQL = $wpdb->query("INSERT INTO " . $wpdb->prefix . "webinar_email_listing 
				(`Listing_Cust_ID`, `Listing_Email_Time`, `Listing_Email_Subject`, `Listing_Email`) VALUES 
				(".$Cust_ID.", '".date('Y-m-d H:i:s', strtotime($SignupDate . '+1 days'))."', 'Email_Drift1_Subject', 'Email_Drift1'), 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (2*60*60))."', 'twohour_Email_Subject', 'twohour_Email'), 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (30*60))."', 'thirtyMinute_Email_Subject', 'thirtyMinute_Email'), 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (5*60))."', 'fiveMinute_Email_Subject', 'fiveMinute_Email')");
			$missedSQL = $wpdb->query("INSERT INTO " . $wpdb->prefix . "webinar_missed_listing 
				(`Missed_Cust_ID`, `Missed_Email_Time`, `Missed_Email_Subject`, `Missed_Email`) VALUES 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) + (60*60))."', 'initial_Missing_Email_subject', 'initial_Missing_Email')");
			
		} else if($daysLive < 2 && $daysLive == 1) {

			$listingSQL = $wpdb->query("INSERT INTO " . $wpdb->prefix . "webinar_email_listing 
				(`Listing_Cust_ID`, `Listing_Email_Time`, `Listing_Email_Subject`, `Listing_Email`) VALUES 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (2*60*60))."', 'twohour_Email_Subject', 'twohour_Email'), 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (30*60))."', 'thirtyMinute_Email_Subject', 'thirtyMinute_Email'), 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (5*60))."', 'fiveMinute_Email_Subject', 'fiveMinute_Email')");
			$missedSQL = $wpdb->query("INSERT INTO " . $wpdb->prefix . "webinar_missed_listing 
				(`Missed_Cust_ID`, `Missed_Email_Time`, `Missed_Email_Subject`, `Missed_Email`) VALUES 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) + (60*60))."', 'initial_Missing_Email_subject', 'initial_Missing_Email')");
			
		} else if($daysLive < 1 && $hoursLive <= 120 ) {

			$listingSQL = $wpdb->query("INSERT INTO " . $wpdb->prefix . "webinar_email_listing 
				(`Listing_Cust_ID`, `Listing_Email_Time`, `Listing_Email_Subject`, `Listing_Email`) VALUES 				
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (30*60))."', 'thirtyMinute_Email_Subject', 'thirtyMinute_Email'), 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) - (5*60))."', 'fiveMinute_Email_Subject', 'fiveMinute_Email')");
			$missedSQL = $wpdb->query("INSERT INTO " . $wpdb->prefix . "webinar_missed_listing 
				(`Missed_Cust_ID`, `Missed_Email_Time`, `Missed_Email_Subject`, `Missed_Email`) VALUES 
				(".$Cust_ID.", '".date("Y-m-d H:i:s", strtotime($WebinarDate) + (60*60))."', 'initial_Missing_Email_subject', 'initial_Missing_Email')");
		}

		if( $listingSQL && $missedSQL ) {			
            $to    	 = $EmailID;
            $subject = $Email_ScheduleDate_Subject;
            $body 	 = strtr( 
				$Email_ScheduleDate, 
				array(
					'#FirstName#' 	=> $FirstName,
					'#LastName#' 	=> $LastName,
					'#LiveDate#' 	=> date('g:i A \o\n l, jS F, Y', strtotime($WebinarDate)),
					'#EncoreDate#' 	=> date('g:i A \o\n l, jS F, Y', strtotime($EncoreDate))
				)
			);
            Send_Mail( $subject, $body, $FirstName.' '.$LastName, $to );
			echo 1;
		} else {
			echo 0;
		}
	} else {
		echo 0;
	}
}

function WebinarUpdateForm() {
	global $wpdb;

	$result = array();
	$UniqueID    = addslashes($_POST['unique_id']);
	$User = $wpdb->get_row("SELECT * FROM `" . $wpdb->prefix . "webinar` WHERE `Cust_Unique_ID` = '" . $UniqueID . "'");
	
	$Website 	 = isset($_POST['website']) ? addslashes($_POST['website']) : '';
	$Company 	 = isset($_POST['company']) ? addslashes($_POST['company']) : '';
	$Designation = isset($_POST['designation']) ? addslashes($_POST['designation']) : '';
	$Phone 		 = empty($User->Cust_Phone) ? addslashes($_POST['phone']) : $User->Cust_Phone;

	if ( !empty($Website) ) {
		$UpdateWebinar = $wpdb->update( 
			$wpdb->prefix . 'webinar',
			array(
				'Cust_Website' 		=> !empty($Website) ? $Website : '',
				'Cust_Company' 		=> !empty($Company) ? $Company : '',
				'Cust_Phone'		=> empty($User->Cust_Phone) ? $Phone : $User->Cust_Phone,
				'Cust_Designation' 	=> !empty($Designation) ? $Designation : '',
			),
			array(
				'Cust_Unique_ID' => $UniqueID
			)
		);
		$result['code'] = 0;
		$result['result'] = 'Your details successfully updated.';
	} else {
		$result['code'] = 1;
		$result['result'] = 'Website field is required.';
	}

	echo json_encode($result);
}

function GetCurrentPHPDateTime() {
	$result['datetime'] = date('Y-m-d H:i:s');

	echo json_encode($result);
}

function BuyNowWebinar() {
	$UniqueID 	= addslashes($_POST['UniqueID']);
	$reflength  = 10;
	$refchars   = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$refcode    = substr( str_shuffle( $refchars ), 0, $reflength );

	if( !empty($UniqueID) ) {
		echo "https://playbookai-test.chargebee.com/hosted_pages/plans/engagewise-webinar?subscription[plan_quantity]=2&customer[cf_ew_token]=".$refcode;
	} else {
		echo 0;
	}
}

function LiveWebinarTime() {
	global $wpdb;
	$webinar_date 	         = isset($_POST['webinar_date']) ? addslashes($_POST['webinar_date']) : '';
	$webinar_current_time 	 = isset($_POST['webinar_current_time']) ? addslashes($_POST['webinar_current_time']) : '';
	$webinar_end_time        = isset($_POST['webinar_end_time']) ? addslashes($_POST['webinar_end_time']) : '';	
	
	$data = $wpdb->get_row("SELECT * FROM `" . $wpdb->prefix . "webinar_Status` WHERE `Webinar_Live_Date` = '".$webinar_date ."'");
	if ( count($data) == 0 ) {
		$wpdb->query("INSERT INTO " . $wpdb->prefix . "webinar_Status (`Webinar_Live_Date`, `Webinar_Current_Time`, `Webinar_End_Time`) VALUES 				
				('".$webinar_date."', '".$webinar_current_time."', ".$webinar_end_time.")");
	} else {
		$wpdb->query("UPDATE `" . $wpdb->prefix . "webinar_Status` SET `Webinar_Current_Time`='".$webinar_current_time."',`Webinar_End_Time`='".$webinar_end_time."' WHERE `Webinar_Live_Date` = '".$webinar_date."'");
	}
}
function MissedWebinar() {
	global $wpdb;

	$UserUniqueID 	= addslashes($_POST['UserUniqueID']);	
	$WebinarStatus  = 'Complete';	

	if( !empty($UserUniqueID) && !empty($WebinarStatus) ) {				
		$UpdateUserStatus = $wpdb->update( 
			$wpdb->prefix . 'webinar',
			array(				
				'Cust_Webinar_Status' => $WebinarStatus,
			),
			array(
				'Cust_Unique_ID' => $UserUniqueID
			)
		);
	}

	$CheckUser = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "webinar WHERE Cust_WebinarDate >= '".date('Y-m-d H:i:s')."' AND Cust_Webinar_Status = 'Unseen'" );
	foreach( $CheckUser as $UserData ) {
		$webinarUserID 		= $UserData->Cust_ID;
		$webinarFirstname	= $UserData->Cust_FirstName;
		$webinarLastname	= $UserData->Cust_LastName;
		$webinarEmail		= $UserData->Cust_Email;
		$webinarFullName	= $webinarFirstname . " " . $webinarLastname;

		$webinar 			= $wpdb->get_row( "SELECT * FROM " . $wpdb->prefix . "webinar_missed_listing WHERE Missed_Cust_ID = ".$webinarUserID );
		$webinarDateTime 	= $webinar->Missed_Email_Time;
		$webinarEmailStatus = $webinar->Missed_Email_Status;		

		if( $webinarEmailStatus == 'NO' && $webinarDateTime >= date("Y-m-d H:i:s") ) {
			$updateSQL = $wpdb->query( "UPDATE " . $wpdb->prefix ."webinar_missed_listing SET Missed_Email_Status = 'YES' WHERE Missed_Email_Time LIKE '".date("Y-m-d")."%' AND Missed_Cust_ID = ".$webinarUserID );
			if( $updateSQL ) {
				$to 		= $webinarEmail;
				$subject 	= $webinar->Missed_Email_Subject;
				$body 		= $webinar->Missed_Email;
				Send_Mail( $subject, $body, $webinarFullName, $to );
				echo "Email just send.<br>";
			} else {
				echo 'Error Sending Email';
			}
		} else {
			echo "Email already Send before.<br>";
		}
	}
	if( $UpdateUserStatus && $updateSQL ) {
		echo 1;
	} else {
		echo 0;
	}
}