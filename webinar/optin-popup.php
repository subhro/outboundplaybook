<?php require( dirname( dirname( __FILE__ ) ) . '/wp-load.php' ); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Optin</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.min.css" rel="stylesheet">
    <style type="text/css">
	#WebinarSignupForm {
	    background-color: #f85;
	    margin: 20px 0;
	    padding: 25px;
	}
	#WebinarSignupForm label {
		color: #fff;
		font-weight: bold;
		font-size: 18px;
	}
	#WebinarSignupForm .form-control {
		border-radius: 0;
		border-color: #e74;
		font-size: 18px;
		height: auto;
	}
	#WebinarSignupForm .btn {
		background-color: #b44613;
	    border-color: #ae400d;
	    font-size: 18px;
	    padding: 8px 20px;
	}
    </style>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>var siteurl = '<?php echo site_url('/webinar'); ?>';</script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-4">
				<form method="post" action="<?php echo site_url('/webinar/ajax/?case=WebinarSignupForm'); ?>" id="WebinarSignupForm">
					<?php echo $error; ?>
					<div class="form-group">
						<label for="first_name">First Name:</label>
						<input class="form-control" type="text" name="first_name" id="first_name" placeholder="Enter First Name" />
					</div>
					<div class="form-group">
						<label for="last_name">Last Name:</label>
						<input class="form-control" type="text" name="last_name" id="last_name" placeholder="Enter Last Name" />
					</div>
					<div class="form-group">
						<label for="email_id">Email ID:</label>
						<input class="form-control" type="email" name="email_id" id="email_id" placeholder="Enter Email ID" />
					</div>
					<div class="form-group">
						<input type="hidden" name="current_url" id="current_url" value="<?php echo $_SERVER['HTTP_REFERER']; ?>" />
						<button type="submit" name="schedule" class="btn btn-primary">Schedule Demo</button>
					</div>
				</form>
			</div>
			<div class="col-sm-8">
				<h2>Join OutboundPlaybook Webinar</h2>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
				<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
			</div>
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.min.js"></script>
	<script src="<?php echo site_url('/webinar/scripts/bootstrap-notify.min.js'); ?>"></script>
	<script type="text/javascript">var siteurl = '<?php echo site_url('/webinar'); ?>';</script>
	<script src="<?php echo site_url('/webinar/scripts/function.js'); ?>"></script>
</body>
</html>