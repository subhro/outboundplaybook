<?php 
include_once( dirname( __FILE__ ) . "/config/mail-process.php" );
require(dirname(dirname(__FILE__)) . '/wp-load.php' );

global $wpdb;
if ( !session_id() ) session_start();

if ( empty($_SESSION['_webinar_data']) ) {
    header("location:" . site_url());
    exit();
} ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="robots" content="noindex">

	<title>Schedule Webinar - OutboundPlaybook</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo site_url('/webinar/styles/fullcalendar.min.css'); ?>" rel="stylesheet" />
	<link href="<?php echo site_url('/webinar/styles/fullcalendar.print.min.css'); ?>" rel="stylesheet" media='print' />
	<link href="<?php echo site_url('/webinar/styles/calendar.css'); ?>" rel="stylesheet" />
</head>
<body>
	<div id="wrap">
		<div class="spinner hide">
		  	<div class="double-bounce1"></div>
		  	<div class="double-bounce2"></div>
		</div>
		<div id="calendar"></div>
		<div style="clear:both"></div>
		<form method="post" id="schedule-form">
			<input type="hidden" name="webinar_live" id="webinar_live" />
			<input type="hidden" name="webinar_encore" id="webinar_encore" />
		</form>
	</div>

	<script src="<?php echo site_url('/webinar/scripts/moment.min.js'); ?>"></script>
	<script src="<?php echo site_url('/webinar/scripts/jquery.min.js'); ?>"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="<?php echo site_url('/webinar/scripts/bootstrap-notify.min.js'); ?>"></script>
	<script src="<?php echo site_url('/webinar/scripts/fullcalendar.min.js'); ?>"></script>
	<script>
	$(document).ready(function() {
		$('#calendar').fullCalendar({
			customButtons: {
				reset: {
					text: 'Reset',
					click: function() {
						window.location.reload();
					}
				},
				confirm: {
					text: 'Confirm',
					click: function() {
						if ( $('#webinar_live').val() != false && $('#webinar_encore').val() != false ) {
							$('.spinner').removeClass('hide');
							var siteurl 		= '<?php echo site_url('/webinar'); ?>';
							var webinarLive 	= $('#webinar_live').val();
							var webinarEncore 	= $('#webinar_encore').val();
							$.ajax({
				                type    : 'POST',
				                cache   : false,
				                url     : siteurl + "/ajax/?case=ScheduleWebinar",
				                data    : {
				                    'LiveDate'		: webinarLive,
				                    'EncoreDate'	: webinarEncore
				                },
				                success : function(data) {
				                    if ( data == 0 ) {
				                    	alert('Something went wrong, please try again.');
				                    } else {
			                        	window.location.replace(siteurl + "/thank-you/");
				                    }
				                    $('.spinner').addClass('hide');
				                }
				            });
						} else if ( $('#webinar_live').val() != false && $('#webinar_encore').val() == false ) {
							$.notify({
		                        icon: 'glyphicon glyphicon-info',
		                        message: 'Choose a ENCORE Webinar date before confirm.',
		                    },{
		                        type: 'danger',
		                        placement: {
		                            from: "top",
		                            align: "right"
		                        }
		                    });
						} else if ( $('#webinar_live').val() == false && $('#webinar_encore').val() != false ) {
							$.notify({
		                        icon: 'glyphicon glyphicon-info',
		                        message: 'Choose a LIVE Webinar date before confirm.',
		                    },{
		                        type: 'danger',
		                        placement: {
		                            from: "top",
		                            align: "right"
		                        }
		                    });
						} else {
							$.notify({
		                        icon: 'glyphicon glyphicon-info',
		                        message: 'Choose a LIVE & ENCORE Webinar date before confirm.',
		                    },{
		                        type: 'danger',
		                        placement: {
		                            from: "top",
		                            align: "right"
		                        }
		                    });
						}
					}
				}
			},
			header: {
				left: 'title',
				center: 'month',
				right: 'today prev,next'
			},
			footer: {
				right: 'reset, confirm'
			},
			firstDay: 1,
			eventClick:  function(calEvent, jsEvent, view) {
				if ( $(this).hasClass('btn-success') ) {
					$('.live-icon').remove();
					$(this).find('.fc-title').html(calEvent.title + ' <span class="glyphicon glyphicon-ok live-icon"></span>');
					$('#webinar_live').val(calEvent.id);
				} else if ( $(this).hasClass('btn-danger') ) {
					$('.encore-icon').remove();
					$(this).find('.fc-title').html(calEvent.title + ' <span class="glyphicon glyphicon-ok encore-icon"></span>');
					$('#webinar_encore').val(calEvent.id);
				}
			},
			events: [
				<?php 
				$today = date('D');
				$hour = date('H'); $minute = date('i');
				if ( $today == 'Mon' ) {
					$ScheduleData = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "webinar_schedule` WHERE `Schedule_DateTime` >= '" . date('Y-m-d H:i:s') . "' AND `Schedule_DateTime` <= '" . date('Y-m-d H:i:s', strtotime("+7 day", strtotime(date('Y-m-d H:i:s')))) . "'");
				} elseif ( $today == 'Tue' ) {
					$ScheduleData = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "webinar_schedule` WHERE `Schedule_DateTime` >= '" . date('Y-m-d H:i:s') . "' AND `Schedule_DateTime` <= '" . date('Y-m-d H:i:s', strtotime("+6 day", strtotime(date('Y-m-d H:i:s')))) . "'");
				} elseif ( $today == 'Wed' ) {
					$ScheduleData = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "webinar_schedule` WHERE `Schedule_DateTime` >= '" . date('Y-m-d') . "' AND `Schedule_DateTime` <= '" . date('Y-m-d', strtotime("+5 day", strtotime(date('Y-m-d')))) . "'");
					if ( $hour > date('H', strtotime($ScheduleData[0]->Schedule_DateTime)) && $minute > date('i', strtotime($ScheduleData[0]->Schedule_DateTime)) ) {
						$ScheduleData = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "webinar_schedule` WHERE `Schedule_DateTime` > '" . date('Y-m-d H:i:s') . "' AND `Schedule_DateTime` <= '" . date('Y-m-d H:i:s', strtotime("+5 day", strtotime(date('Y-m-d H:i:s')))) . "'");
					}
				} elseif ( $today == 'Thu' ) {
					$ScheduleData = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "webinar_schedule` WHERE `Schedule_DateTime` >= '" . date('Y-m-d H:i:s') . "' AND `Schedule_DateTime` <= '" . date('Y-m-d H:i:s', strtotime("+4 day", strtotime(date('Y-m-d H:i:s')))) . "'");
				} elseif ( $today == 'Fri' ) {
					$ScheduleData = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "webinar_schedule` WHERE `Schedule_DateTime` >= '" . date('Y-m-d') . "' AND `Schedule_DateTime` <= '" . date('Y-m-d', strtotime("+3 day", strtotime(date('Y-m-d')))) . "'");
					if ( $hour > date('H', strtotime($ScheduleData[0]->Schedule_DateTime)) && $minute > date('i', strtotime($ScheduleData[0]->Schedule_DateTime)) ) {
						$ScheduleData = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "webinar_schedule` WHERE `Schedule_DateTime` > '" . date('Y-m-d H:i:s', strtotime("+3 day", strtotime(date('Y-m-d H:i:s')))) . "' AND `Schedule_DateTime` <= '" . date('Y-m-d H:i:s', strtotime("+9 day", strtotime(date('Y-m-d H:i:s')))) . "'");
					}
				} elseif ( $today == 'Sat' ) {
					$ScheduleData = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "webinar_schedule` WHERE `Schedule_DateTime` >= '" . date('Y-m-d H:i:s', strtotime("+2 day", strtotime(date('Y-m-d H:i:s')))) . "' AND `Schedule_DateTime` <= '" . date('Y-m-d H:i:s', strtotime("+9 day", strtotime(date('Y-m-d H:i:s')))) . "'");
				} elseif ( $today == 'Sun' ) {
					$ScheduleData = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "webinar_schedule` WHERE `Schedule_DateTime` >= '" . date('Y-m-d H:i:s', strtotime("+1 day", strtotime(date('Y-m-d H:i:s')))) . "' AND `Schedule_DateTime` <= '" . date('Y-m-d H:i:s', strtotime("+8 day", strtotime(date('Y-m-d H:i:s')))) . "'");
				}
				foreach ( $ScheduleData as $Schedule ) { ?>
					{
						id: <?php echo $Schedule->Schedule_ID; ?>,
						title: "<?php echo $Schedule->Schedule_Date_Type; ?> Webinar",
						allDay: false,
						start: "<?php echo date('Y-m-d H:i:s', strtotime($Schedule->Schedule_DateTime)); ?>",
						className: "<?php echo $Schedule->Schedule_Date_Type=='Live'?'btn btn-success':'btn btn-danger'; ?>"
					},
				<?php } ?>
			]
		});
	});
	</script>
</body>
</html>